#! /bin/bash
datadir='/Users/dqwang/Study/research/nlp4fe/workspace/data/gics50'
svm_learn='/Users/dqwang/Study/research/nlp4fe/svm_perf/svm_perf_learn'
#svm_learn='/Users/dqwang/Study/research/nlp4fe/svm_light/svm_learn'
svm_classify='/Users/dqwang/Study/research/nlp4fe/svm_perf/svm_perf_classify'
#svm_classify='/Users/dqwang/Study/research/nlp4fe/svm_light/svm_classify'
codedir='/Users/dqwang/Study/research/nlp4fe/workspace/code'
modellindir='/Users/dqwang/Study/research/nlp4fe/workspace/modellin'
modeldir='/Users/dqwang/Study/research/nlp4fe/workspace/model'
outputdir='/Users/dqwang/Study/research/nlp4fe/workspace/exp'
cd ${codedir}
echo -e 'Label_Name\tPred_N\tLab_N\tPrecision\tRecall\tF-score\tMCC\tAccuracy'
for task in 'bigchange'
do
	echo ${task} 
	for fea in 'tof_bofte_bow_tf' 'tof_bofte_bow_tf_pp' 'tof_bofte_bow_tfidf' 'tof_bofte_bow_tfidf_pp'
	do
		echo ${fea}
		echo 'linearizing train'
#		rm ${modellindir}/dictionary.dict
		python3 -m flink.linearize.ksl -o ${modellindir} -k STK ${datadir}/${task}/reuters_2008_gics50_p1y_stockprice_lag0_duration2_${task}2pct_${fea}_train.data > ${outputdir}/log.txt
		python3 -m flink.linearize.binary -t 10000000 -f 0 -k ${modellindir} -o ${modellindir} ${datadir}/${task}/reuters_2008_gics50_p1y_stockprice_lag0_duration2_${task}2pct_${fea}_train.data ${datadir}/${task}/reuters_2008_gics50_p1y_stockprice_lag0_duration2_${task}2pct_${fea}_test.data > ${outputdir}/log.txt
#		python3 -m flink.linearize.binary -t 1000000 -f 0 -k ${modellindir} -o ${modellindir} ${datadir}/${task}/reuters_2008_gics50_p1y_stockprice_lag0_duration2_${task}2pct_${fea}_train.data ${datadir}/${task}/reuters_2008_gics50_p1y_stockprice_lag0_duration2_${task}2pct_${fea}_train.data > ${outputdir}/log.txt
		cp ${modellindir}/dictionary.dict.txt ${datadir}/dictionary.dict
		cp ${modellindir}/reuters_2008_gics50_p1y_stockprice_lag0_duration2_${task}2pct_${fea}_train.data.lin ${datadir}/
		cp ${modellindir}/reuters_2008_gics50_p1y_stockprice_lag0_duration2_${task}2pct_${fea}_test.data.lin ${datadir}/
		python ${codedir}/mergeAttr.py ${datadir}/${task}/reuters_2008_gics50_p1y_stockprice_lag0_duration2_${task}2pct_${fea}_train_attributes.txt ${datadir}/dictionary.dict ${datadir}/reuters_2008_gics50_p1y_stockprice_lag0_duration2_${task}2pct_${fea}_train.all.attr ${datadir}/idMap.txt 
		python ${codedir}/genVecLib.py ${datadir}/${task}/reuters_2008_gics50_p1y_stockprice_lag0_duration2_${task}2pct_${fea}_train.data ${datadir}/reuters_2008_gics50_p1y_stockprice_lag0_duration2_${task}2pct_${fea}_train.data.lin ${datadir}/idMap.txt ${datadir}/reuters_2008_gics50_p1y_stockprice_lag0_duration2_${task}2pct_${fea}_train.all.vec 
	#	echo 'linearizing test'
#		cp ${modellindir}/dictionary.dict.txt ${datadir}/dictionary.dict
		python ${codedir}/mergeAttr.py ${datadir}/${task}/reuters_2008_gics50_p1y_stockprice_lag0_duration2_${task}2pct_${fea}_train_attributes.txt ${datadir}/dictionary.dict ${datadir}/reuters_2008_gics50_p1y_stockprice_lag0_duration2_${task}2pct_${fea}_test.all.attr ${datadir}/idMap.txt 
		python ${codedir}/genVecLib.py ${datadir}/${task}/reuters_2008_gics50_p1y_stockprice_lag0_duration2_${task}2pct_${fea}_test.data ${datadir}/reuters_2008_gics50_p1y_stockprice_lag0_duration2_${task}2pct_${fea}_test.data.lin ${datadir}/idMap.txt ${datadir}/reuters_2008_gics50_p1y_stockprice_lag0_duration2_${task}2pct_${fea}_test.all.vec 
		echo 'classification'
		${svm_learn} -c 1 ${datadir}/reuters_2008_gics50_p1y_stockprice_lag0_duration2_${task}2pct_${fea}_train.all.vec ${modeldir}/model_allvec_${fea} > ${outputdir}/log.txt
		${svm_classify} ${datadir}/reuters_2008_gics50_p1y_stockprice_lag0_duration2_${task}2pct_${fea}_test.all.vec ${modeldir}/model_allvec_${fea} ${outputdir}/pred_allvec_${fea}.txt > ${outputdir}/log.txt
		python ${codedir}/eval.py ${outputdir}/pred_allvec_${fea}.txt ${datadir}/${task}/reuters_2008_gics50_p1y_stockprice_lag0_duration2_${task}2pct_${fea}_test.data ''
	done
done
cd -

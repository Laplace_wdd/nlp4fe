#! /bin/bash
datadir='/Users/dqwang/Study/research/nlp4fe/workspace/data'
svm_learn='/Users/dqwang/Study/research/nlp4fe/svm_perf/svm_perf_learn'
svm_classify='/Users/dqwang/Study/research/nlp4fe/svm_perf/svm_perf_classify'
svm_learn_tk='/Users/dqwang/Study/research/nlp4fe/svm-light-TK-1_2_1/svm_learn'
svm_classify_tk='/Users/dqwang/Study/research/nlp4fe/svm-light-TK-1_2_1/svm_classify'
codedir='/Users/dqwang/Study/research/nlp4fe/workspace/code'
modeldir='/Users/dqwang/Study/research/nlp4fe/workspace/model'
outputdir='/Users/dqwang/Study/research/nlp4fe/workspace/exp'
for task in 'bigchange' 'polarity'
do
	echo ${task}
	for fea in 'bow_tf' 'bow_tf_pp' 'bow_tfidf_pp' 'bofte_tf' 'bofte_tf_pp' 'bofte_tfidf' 'bofte_tfidf_pp' 'bof_tf' 'bof_tf_pp' 'bof_tfidf' 'bof_tfidf_pp' 'bofte_bow_tf' 'bofte_bow_tf_pp' 'bofte_bow_tfidf' 'bofte_bow_tfidf_pp'
	do
		echo ${fea}
		${svm_learn} -c 100 ${datadir}/${task}/reuters_2008_gics50_p1y_stockprice_lag0_duration2_${task}2pct_${fea}_train.data ${modeldir}/model_${fea} > ${outputdir}/log.txt 
		${svm_classify} ${datadir}/${task}/reuters_2008_gics50_p1y_stockprice_lag0_duration2_${task}2pct_${fea}_test.data ${modeldir}/model_${fea} ${outputdir}/pred_${fea}.txt > ${outputdir}/log.txt 
		python ${codedir}/eval.py ${outputdir}/pred_${fea}.txt ${datadir}/${task}/reuters_2008_gics50_p1y_stockprice_lag0_duration2_${task}2pct_${fea}_test.data
	done
	for fea in 'tof_bofte_bow_tf' 'tof_bofte_bow_tf_pp' 'tof_bofte_bow_tfidf' 'tof_bofte_bow_tfidf_pp'
	do
		echo ${fea}
		${svm_learn_tk} -t 5 -D 0 -S 1 -d 1 -C + -c 1 ${datadir}/${task}/reuters_2008_gics50_p1y_stockprice_lag0_duration2_${task}2pct_${fea}_train.data ${modeldir}/model_${fea} > ${outputdir}/log.txt
		${svm_classify_tk} -f 1 ${datadir}/${task}/reuters_2008_gics50_p1y_stockprice_lag0_duration2_${task}2pct_${fea}_test.data ${modeldir}/model_${fea} ${outputdir}/pred_${fea}.txt > ${outputdir}/log.txt
	python ${codedir}/eval.py ${outputdir}/pred_${fea}.txt ${datadir}/${task}/reuters_2008_gics50_p1y_stockprice_lag0_duration2_${task}2pct_${fea}_test.data
	done
done

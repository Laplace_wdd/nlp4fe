#! /bin/bash
for SEC in 'gics30' 'gics45' 'gics50'
do
	for TASK in 'polarity' 'bigchange' 
	do
		for YEAR in '2008' '2009' '2010' '2011' '2012' 
		do
			export year=${YEAR}
			export sec=${SEC}
			export task=${TASK}
			for FEA in 'bow_tf' 'bow_tf_pp' 'bofte_tf' 'bofte_tf_pp' 'bof_tf' 'bof_tf_pp' 'bofte_bow_tf' 'bofte_bow_tf_pp'
			do
				export fea=${FEA}
				./nor_core.sh
			done
			for FEA in 'tof_bofte_bow_tf' 'tof_bofte_bow_tf_pp'
			do
				export fea=${FEA}
				#./tk_core.sh
				#./lin_core.sh
			done
		done
	done
done

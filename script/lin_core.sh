#/bin/bash
datadir='/Users/dqwang/Study/research/nlp4fe/workspace/data'
svm_learn='/Users/dqwang/Study/research/nlp4fe/svm_perf/svm_perf_learn'
svm_classify='/Users/dqwang/Study/research/nlp4fe/svm_perf/svm_perf_classify'
codedir='/Users/dqwang/Study/research/nlp4fe/workspace/code'
modeldir='/Users/dqwang/Study/research/nlp4fe/workspace/model'
modellindir=${modeldir}/modellin
outputdir='/Users/dqwang/Study/research/nlp4fe/workspace/exp'
#echo -e 'Feature_Setting,Label_Name,Pred_N,Lab_N,Precision,Recall,F-score,MCC,Accuracy' > ${outputdir}/res.csv 
pref=reuters_${year}_${sec}_p1y_stockprice_lag0_duration2_${task}2pct	
prefix=${pref}_${fea}
d_prefix=${datadir}/${pref}/${prefix}
f_prefix=${datadir}/${pref}
if [ ! -f ${d_prefix}_train.data ]
then
        exit 0
fi
if [ ! -d ${modellindir} ]
then
       mkdir ${modellindir}
fi
if [ -d ${modeldir}/${prefix}_lin ]
then
       rm -r ${modeldir}/${prefix}_lin 
fi
echo ${prefix}_lin
#echo 'linearizing train'
date_start=$(date +%s)
echo "flink.linearize.ksl"
python3 -m flink.linearize.ksl -s 10 -o ${modellindir} -k STK ${d_prefix}_train.data #>> ${outputdir}/${prefix}_log.txt
echo "flink.linearize.binary"
python3 -m flink.linearize.binary -t 100000000 -f 0 -k ${modellindir} -o ${modellindir} ${d_prefix}_train.data ${d_prefix}_test.data #>> ${outputdir}/${prefix}_log.txt
mv ${modellindir} ${modeldir}/${prefix}_lin
modellindir=${modeldir}/${prefix}_lin
cp ${modellindir}/dictionary.dict.txt ${d_prefix}.dict
cp ${modellindir}/${prefix}_train.data.lin ${f_prefix}/
cp ${modellindir}/${prefix}_test.data.lin ${f_prefix}/
python ${codedir}/mergeAttr.py ${d_prefix}_train_attributes.txt ${d_prefix}.dict ${d_prefix}_train.all.attr ${d_prefix}_train.idMap
python ${codedir}/genVecLib.py ${d_prefix}_train.data ${d_prefix}_train.data.lin ${d_prefix}_train.idMap ${d_prefix}_train.all.vec 
python ${codedir}/mergeAttr.py ${d_prefix}_train_attributes.txt ${d_prefix}.dict ${d_prefix}_test.all.attr ${d_prefix}_test.idMap
python ${codedir}/genVecLib.py ${d_prefix}_test.data ${d_prefix}_test.data.lin ${d_prefix}_test.idMap ${d_prefix}_test.all.vec
${svm_learn} -c 0.1 ${d_prefix}_train.all.vec ${modeldir}/${prefix}_model_allvec >> ${outputdir}/${prefix}_log.txt
${svm_classify} ${d_prefix}_test.all.vec ${modeldir}/${prefix}_model_allvec ${outputdir}/${prefix}_pred_allvec.txt >> ${outputdir}/${prefix}_log.txt
python ${codedir}/eval.py ${outputdir}/${prefix}_pred_allvec.txt ${d_prefix}_test.data ${prefix}_lin > ${outputdir}/${prefix}_allvec_eval.csv svmlight
cat ${outputdir}/${prefix}_allvec_eval.csv
#python ${codedir}/eval_rank.py ${outputdir}/${prefix}_pred_allvec.txt ${d_prefix}_test.data ${prefix}_lin > ${outputdir}/${prefix}_allvec_eval_rank.csv
#cat ${outputdir}/${prefix}_allvec_eval_rank.csv
#date_end=$(date +%s)
echo "Processing Time:$((date_end-date_start))sec" >> ${outputdir}/${prefix}_log.txt

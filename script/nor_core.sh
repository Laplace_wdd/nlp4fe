#! /bin/bash
datadir='/Users/dqwang/Study/research/nlp4fe/workspace/data'
svm_learn='/Users/dqwang/Study/research/nlp4fe/svm_perf/svm_perf_learn'
svm_classify='/Users/dqwang/Study/research/nlp4fe/svm_perf/svm_perf_classify'
codedir='/Users/dqwang/Study/research/nlp4fe/workspace/code'
modeldir='/Users/dqwang/Study/research/nlp4fe/workspace/model'
outputdir='/Users/dqwang/Study/research/nlp4fe/workspace/exp'

pref=reuters_${year}_${sec}_p1y_stockprice_lag0_duration2_${task}2pct	
prefix=${pref}_${fea}
d_prefix=${datadir}/${pref}/${prefix}
f_prefix=${datadir}/${pref}
if [ ! -f ${d_prefix}_train.data ]
then
        exit 0
fi
echo ${prefix}
date_start=$(date +%s)
${svm_learn} -c 100 ${d_prefix}_train.data ${modeldir}/${prefix}_model > ${outputdir}/${prefix}_log.txt 
${svm_classify} ${d_prefix}_test.data ${modeldir}/${prefix}_model ${outputdir}/${prefix}_pred.txt > ${outputdir}/${prefix}_log.txt 
python ${codedir}/eval.py ${outputdir}/${prefix}_pred.txt ${d_prefix}_test.data ${prefix} > ${outputdir}/${prefix}_eval.csv 
cat ${outputdir}/${prefix}_eval.csv 
python ${codedir}/eval_rank.py ${outputdir}/${prefix}_pred.txt ${d_prefix}_test.data ${prefix} > ${outputdir}/${prefix}_eval_rank.csv 
cat ${outputdir}/${prefix}_eval_rank.csv 
date_end=$(date +%s)
echo "Processing Time:$((date_end-date_start))sec" >> ${outputdir}/${prefix}_log.txt

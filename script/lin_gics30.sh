#! /bin/bash
datadir='/Users/dqwang/Study/research/nlp4fe/workspace/data/gics30'
svm_learn='/Users/dqwang/Study/research/nlp4fe/svm_perf/svm_perf_learn'
#svm_learn='/Users/dqwang/Study/research/nlp4fe/svm_light/svm_learn'
svm_classify='/Users/dqwang/Study/research/nlp4fe/svm_perf/svm_perf_classify'
#svm_classify='/Users/dqwang/Study/research/nlp4fe/svm_light/svm_classify'
codedir='/Users/dqwang/Study/research/nlp4fe/workspace/code'
modellindir='/Users/dqwang/Study/research/nlp4fe/workspace/modellin'
modeldir='/Users/dqwang/Study/research/nlp4fe/workspace/model'
outputdir='/Users/dqwang/Study/research/nlp4fe/workspace/exp'
#echo -e 'Feature_Setting,Label_Name,Pred_N,Lab_N,Precision,Recall,F-score,MCC,Accuracy' > ${outputdir}/res.csv 
for year in '2010' #'2009' '2010' 
do
	for sec in 'gics30' #'gics45' 'gics50'
	do
		for task in 'bigchange' #'polarity'
		do
			echo ${task} 
			for fea in 'tof_bofte_bow_tf_pp' #'tof_bofte_bow_tfidf_pp' 'tof_bofte_bow_tf'  'tof_bofte_bow_tfidf' 
			do
				echo ${fea}
				echo 'linearizing train'
				pref=reuters_${year}_${sec}_p1y_stockprice_lag0_duration2_${task}2pct	
				prefix=${pref}_${fea}
				d_prefix=${datadir}/${pref}/${prefix}
				f_prefix=${datadir}/${pref}
#				python3 -m flink.linearize.ksl -o ${modellindir} -k STK ${d_prefix}_train.data #>> ${outputdir}/${prefix}_log.txt
#				python3 -m flink.linearize.binary -t 10000000 -f 0 -k ${modellindir} -o ${modellindir} ${d_prefix}_train.data ${d_prefix}_test.data #>> ${outputdir}/${prefix}_log.txt
#				cp ${modellindir}/dictionary.dict.txt ${f_prefix}/dictionary.dict
#				cp ${modellindir}/${prefix}_train.data.lin ${f_prefix}/
#				cp ${modellindir}/${prefix}_test.data.lin ${f_prefix}/
#				python ${codedir}/mergeAttr.py ${d_prefix}_train_attributes.txt ${f_prefix}/dictionary.dict ${d_prefix}_train.all.attr ${f_prefix}/idMap.txt 
#				python ${codedir}/genVecLib.py ${d_prefix}_train.data ${d_prefix}_train.data.lin ${f_prefix}/idMap.txt ${d_prefix}_train.all.vec 
#				python ${codedir}/mergeAttr.py ${d_prefix}_train_attributes.txt ${f_prefix}/dictionary.dict ${d_prefix}_test.all.attr ${f_prefix}/idMap.txt 
#				python ${codedir}/genVecLib.py ${d_prefix}_test.data ${d_prefix}_test.data.lin ${f_prefix}/idMap.txt ${d_prefix}_test.all.vec 
#				echo 'classification'
#				${svm_learn} -c 1 ${d_prefix}_train.all.vec ${modeldir}/${prefix}_model_allvec >> ${outputdir}/${prefix}_log.txt
#				${svm_classify} ${d_prefix}_test.all.vec ${modeldir}/${prefix}_model_allvec ${outputdir}/${prefix}_pred_allvec.txt >> ${outputdir}/${prefix}_log.txt
				#python ${codedir}/eval.py ${outputdir}/${prefix}_pred_allvec.txt ${d_prefix}_test.data ${prefix} > ${outputdir}/${prefix}_res.csv
				python ${codedir}/eval_rank.py ${outputdir}/${prefix}_pred_allvec.txt ${d_prefix}_test.data ${prefix} > ${outputdir}/${prefix}_res.csv
				cat ${outputdir}/${prefix}_res.csv
			done
		done
	done
done

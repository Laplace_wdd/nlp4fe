import sys, re, string, pickle
from itertools import *

def main():
	attrRankLstF = open(sys.argv[1], 'r')
	dataInVecF = open(sys.argv[2], 'r')
	dataOutVecF = open(sys.argv[3], 'w')
	genNewFeaId(attrRankLstF, dataInVecF, dataOutVecF)
	attrRankLstF.close()
	dataInVecF.close()
	dataOutVecF.close()

def genNewFeaId(attrRankLstF, dataInVecF, dataOutVecF):
	rate = float(sys.argv[4])
	attrp = re.compile(r' (\S+)\s+(\d+) .*')
	linp = re.compile(r'(1|-1) (.*)')
	dictp = re.compile('(\d+):(.*)');
	cnt = 0
	topFea = set()
	for l in attrRankLstF:
		m = attrp.match(l)
		if m:
			score = float(m.group(1))
			if score == 0.0:break
			topFea.add(m.group(2))
	for l in dataInVecF:
		m = linp.match(l) 
		lab = m.group(1)
		elemLst = m.group(2).split()
		selLst = []
		for fp in elemLst:
			fid = int(dictp.match(fp).group(1))
			val = float(dictp.match(fp).group(2))
			if fid in topFea:
				selLst += [(fid,2*val*rate)]
			else:
				selLst += [(fid,2*val*(1-rate))]
		print >> dataOutVecF, '{0} {1}'.format(lab, ' '.join(['{0}:{1}'.format(str(fid), str(val)) for fid, val in selLst]))
	
if __name__ == "__main__": main()

"""
**********************************************************
System level configuration options: :mod:`flink.config`
**********************************************************

Yep, it's bad practice... but to reduce a little the number of parameters
passed around, the |project| architecture makes use of some global values. 
Global configuration options have been kept to a minimum, and are
only used for non-critical aspects of the architecture, such as the verbosity
of the output produced by the functions in :mod:`flink.utils.logging`.

The default values of these configuration properties are stored in the
:mod:`flink.config` module. Programs can access and change property values 
by means of the API provided in :mod:`flink.utils.props`.
"""

from configparser import SafeConfigParser
from os import path

logging_verbosity = 4
""" Verbosity of :mod:`flink.utils.logging` messages.

Possible values are:

   1. only print error messages
   2. also print warning messages
   3. also print result messages 
   4. also print information messages **[default]**
   5. also print debugging statements
"""

pytk_use_native = False
"""Used by :mod:`flink.pytk` to decide whether to use the native C layer 
for learning and classification, instead of invoking ``svm_learn`` and
``svm_classify`` executables. Defaults to :keyword:`False`.
"""

class FlinkConfError(BaseException):
   pass

class FlinkConf(SafeConfigParser):

   def __init__(self, fname):
      SafeConfigParser.__init__(self)
      self.read(fname)
      self.sanity_check()

   def sanity_check(self):
      for cname in self.get_classes():
         for what in ["training", "test"]:
            self.get_data(cname, what)
      print("Conf all right")

   def get_classes(self):
      try:
         return self.class_names
      except:
         self.class_names = set(self.get("dataset", "classes").split())
         return self.class_names

   def get_data(self, cname, what):
      if cname in self.class_names:
         file = self.get("dataset", what, vars = {"class": cname}).replace("@[CLASS]", cname)
         if not path.exists(file):
            raise FlinkConfError("File does not exist: {0}".format(file))
         return file
      else:
         raise FlinkConfError("Undefined class: {0}".format(cname))

if __name__ == "__main__":
   parser = FlinkConf("QC.ini")
   for classname in parser.get_classes():
      for what in ["training", "test"]:
         print(parser.get_data(classname, what))

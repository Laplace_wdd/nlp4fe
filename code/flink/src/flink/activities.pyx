"""
****************************************************************
Activities of the linearization process: :mod:`flink.activities`
****************************************************************

Functions in this package implement activities of the linearization
process.

Activities are the building blocks that can be combined to build
sophisticated linearization systems.

Examples of complete linearization systems can be found in 
:mod:`flink.linearize`.

.. note:: Understanding linearization activities

   The description of the linearization activities requires some
   basic understandinf of how the process works and its variants.
   
   For more informations about these topics, please refer to our
   papers:
   
   - `On Reverse Feature Engineering of Syntactic Tree Kernels
     <http://disi.unitn.it/moschitti/articles/CONLL2010-RKE.pdf>`_
     (CoNLL 2010)
   
   - `Reverse Engineering of Tree Kernel Feature Spaces 
     <http://disi.unitn.it/~moschitt/articles/EMNLP09-RK.pdf>`_
     (EMNLP 2009)
   
   - `Efficient Linearization of Tree Kernel Functions
     <http://disi.unitn.it/~moschitt/articles/CONLL2009.pdf>`_
     (CoNLL 2009)
     
.. note:: Parallel activities

   All the activities that can be parallelized implicitly implement
   parallelization. The parameter controlling the number of CPUs
   used is consistently named *ncpu*.

"""

from flink.utils import fileutils, persistency
from flink.utils.logging import info_msg
from os.path import join, basename, dirname
from flink.private.activities import _linearize_file, _classify, \
   _learn_model, _merge_dictionaries
from flink.utils.pool import workers_pool
from flink import pytk
from flink.utils.mathutils import dense_vector
from flink.pytk.miners import greedy_miner
import re

from flink.pytk.defs.FragIdx cimport FragIdx, IdxNode

def learn(outdir, train, num_splits = 1, ncpu = 1, get_etime = False, **learn_options):
   splits = fileutils.split_file(train, num_splits, outdir, balanced = True)
   pool = workers_pool(ncpu)
   for train_split in splits:
      args = {"data": train_split,
              "learn_options": learn_options}
      pool.submit_job(_learn_model, args)
   result = pool.get_results()
   if len(splits) == 1:
      result = result[0]
   if get_etime:
      return result, pool.get_elapsed_time()
   return result

def mine(outdir, models, threshold, min_frequency, ncpu = 1, get_etime = False):
   pool = workers_pool(ncpu)
   if type(models) == str:
      models = [models]
   for model in models:
      dictionary = model + ".dict"
      args = {"model": model,
              "threshold": threshold,
              "min_frequency": min_frequency,
              "dictionary": dictionary}
      pool.submit_job(greedy_miner, args)
   dictionaries = pool.get_results()
   outdict = join(outdir, "dictionary.dict")
   _merge_dictionaries(dictionaries, outdict)
   if get_etime:
      return outdict, pool.get_elapsed_time()
   return outdict


def linearize_file(outdir, dictionary, infile, ncpu = 1, get_etime = False, use_gradient_components = False, kernel_params = None):
   pool = workers_pool(ncpu)
   splits = fileutils.split_file(infile, ncpu, outdir, balanced = False)
   if use_gradient_components:
      if kernel_params is None: 
         #assume infile is a model itself
         kernel_params = pytk.parse_parameters(infile)
   for split in splits:
      outfile = split + ".lin"
      args = {"infile": split, 
              "dictionary": dictionary,
              "outfile": outfile,
              "use_gradient_components": use_gradient_components,
              "kernel_params": kernel_params} 
      pool.submit_job(_linearize_file, args)
   results = pool.get_results()
   linearized_splits = [x[0] for x in results]
   mappings = [x[1] for x in results]
   nid_map = persistency.load(mappings[0])
   for mapping_file in mappings[1:]:
      for x,y in persistency.load(mapping_file).items():
         if x not in nid_map:
            nid_map[x] = y
         else:
            nid_map[x].update(y)
   outfile = join(outdir, basename(infile) + ".lin")
   mapping_file = outfile + ".nid_map"
   persistency.save(nid_map, mapping_file)
   fileutils.join_files(linearized_splits, outfile)
   if get_etime:
      return  outfile, mapping_file, pool.get_elapsed_time()
   return outfile, mapping_file

def classify(datafile, model, ncpu = 1, predictions = None, outdir = None, get_etime = False):
   if outdir is None:
      outdir = dirname(datafile)
   if predictions is None:
      predictions = join(outdir, basename(datafile) + ".pred")
   pool = workers_pool(ncpu)
   splits = fileutils.split_file(datafile, ncpu, outdir)
   for split in splits:
      args = {"datafile": split, "model": model}
      pool.submit_job(_classify, args)
   split_predictions = pool.get_results()
   fileutils.join_files(split_predictions, predictions)
   if get_etime:
      return predictions, pool.get_elapsed_time()
   return predictions

def evaluate(predictions, oracle, store = None, multiclass = False):
   tp = fp = fn = tn = precision = recall = f1 = 0
   with open(oracle) as oraclein:
      with open(predictions) as predin:
         for o in oraclein:
            o = o.strip().split()[0]
            p = predin.readline().strip().split()[0]
            if not multiclass:
               label = float(o)
               pred = float(p)
               if label > 0 and pred > 0: tp += 1
               elif label > 0 and pred < 0: fn += 1
               elif label < 0 and pred > 0: fp += 1
               else: tn += 1
            else:
               if o == p: tp += 1
               else:
                  fp += 1
                  fn += 1
   if tp > 0:
      precision, recall = tp/(tp+fp), tp/(tp+fn)
      f1 = 2 * precision * recall / (precision + recall)
   results = {"true_positives": tp, 
              "false_positives": fp, 
              "false_negatives": fn, 
              "true_negatives": tn, 
              "precision": round(precision*100, 2), 
              "recall": round(recall*100, 2), 
              "f1": round(f1*100, 2)}
   if store is not None:
      with open(store, "w") as dest:
         for k,v in results.items():
            dest.write("{0:20}: {1}\n".format(k.upper(),v))
   return results

def sort_features(dictionary, linearized_model, nid_map, outfile = None):
   example_patt = re.compile(r"^(-?[0-9eE]+(\.[0-9eE]+)?) ((\d+:\d+\s?)+) |EV|$")
   if outfile is None:
      outfile = linearized_model + ".best_frags"
   dictfile = dictionary + ".txt"
   outdict = open(dictfile, "w") 
   nid_mapping = persistency.load(nid_map)
   cdef FragIdx fragidx = persistency.load(dictionary)
   reversedict = {}
   cdef IdxNode x
   for x in fragidx.generator():
      outdict.write("{0}:{1}\n".format(x.nid, x.as_tree()))
      if x.nid not in nid_mapping:
         nids = [x.nid]
      else:
         nids = nid_mapping[x.nid]
      for nid in nids:
         reversedict[nid] = x.as_tree()
   scores = {}
   outdict.close()
   with open(linearized_model) as model:
      for line in model:
         line = line.strip()
         match = example_patt.match(line)
         if match != None:
            score = float(match.group(1))
            features = match.group(3).split()
            for feat in features:
               (id, count) = feat.split(":")
               if int(id) == 1:
                  continue
               key = reversedict[int(id)]
               if key not in scores:
                  scores[key] = 0
               scores[key] += score * int(count)
   pairs = [ (t, scores[t]) for t in scores ]
   pairs.sort(key = lambda a : -a[1])
   with open(outfile, "w") as outf:
      for (t,v) in pairs:
         outf.write("{0} {1}\n".format(v, t))
   return outfile

def calculate_linear_model_norm(modelfile):
   w = dense_vector() # the gradient
   for label,data in pytk.parse_linear_examples(modelfile):
      label = float(label)
      vec = dense_vector.parse(data)
      vec.multiply(label)
      w.sum(vec)
   w[1] = 0 # because in the dictionary we use "1:1" for no feature!
   return round(w.norm(), 2)

def count_frags_in_model(tk_modelfile):
   min = None
   max = sum = 0
   params = pytk.parse_parameters(tk_modelfile)
   for example in pytk.parse_examples(tk_modelfile, True):
      tree = example.trees[0]
      try:
         frags = pytk.count_fragments(tree, params)
         if min is None or min > frags:
            min = frags
         if max < frags:
            max = frags
         sum += frags
      except:
         pass
   return {"min": int(min), "max": int(max), "sum": int(sum)}

def tk2lin(tk_model, dictionary, outdir = None, outfile = None, ncpu = 1, get_etime = False):
   if outfile is None:
      if outdir is None:
         outdir = dirname(tk_model)
      outfile = join(outdir, basename(tk_model) + ".tk2lin")
      
   linearized_tk_model, nid_mapping, elapsed = linearize_file(outdir, dictionary, tk_model, 
      ncpu = ncpu, use_gradient_components = True, 
      get_etime = True)
         
   with open(linearized_tk_model, "r") as infile:
      with fileutils.open_for_writing(outfile, "w") as outdesc:
         for line in infile:
            line = line.strip()
            if line.endswith("# kernel type"):
               outdesc.write("1 # kernel type\n")
            elif line.endswith("# kernel parameter -d"):
               outdesc.write("1 # kernel parameter -d\n")
            elif line.endswith("# kernel parameter -N"):
               outdesc.write("0 # kernel parameter -N\n")
            else:
               outdesc.write(line)
               outdesc.write("\n")
   if get_etime:
      return (outfile, elapsed)
   return outfile 

def get_tk_model_norm(tk_model):
   return pytk.get_tk_model_norm(tk_model)

def find_models(modelsdir):
   from os import listdir
   result = []
   kernel_name = None
   for filename in listdir(modelsdir):
      if filename.split(".")[-1].startswith("model"):
         model = join(modelsdir, filename)
         params = pytk.parse_parameters(model)
         if kernel_name is None:
            kernel_name = params.kernel_name
         else:
            if kernel_name != params.kernel_name:
               raise RuntimeError("Mismatching models in directory "
                  "{0}: {1} and {2}".format(
                     modelsdir, kernel_name, params.kernel_name))
         result.append(model)
   return sorted(result), kernel_name

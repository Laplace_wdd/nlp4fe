from flink.activities import mine, linearize_file, \
   classify, evaluate, learn
from os.path import join
from flink.utils import fileutils
from flink.utils.logging import info_msg
from math import ceil, log10
from flink.utils.pool import workers_pool

def _get_threshold_values(lowerbound, upperbound):
   oom = range(int(log10(lowerbound))-1, int(log10(upperbound))+1)
   result = []
   for order in oom:
      for factor in [2.5, 5, 7.5, 10]:
         value = (10**order) * factor
         if value > upperbound:
            return result
         if value >= lowerbound:
            result.append(value)

def _thropt_evaluate_in(threshold, outdir, num_folds, benchmark, models):
   dictionary = mine(outdir, models, threshold, 3, 1)
   linearized_benchmark,nid_mapping = linearize_file(outdir, dictionary, benchmark)
   folds = fileutils.make_folds(linearized_benchmark, num_folds, outdir)
   results = []
   for train,test in folds.items():
      models = learn(outdir, train, kernel_type = "linear")
      predictions = classify(test, models, 1)
      f1 = evaluate(predictions, test)["f1"]
      results.append(f1)
   avg = sum(results) / len(results)
   info_msg("Average F1 for threshold {1}: {0}".format(avg, threshold))
   return {"dictionary": dictionary,
           "linearized_benchmark": linearized_benchmark,
           "nid_mapping": nid_mapping,
           "threshold": threshold,
           "avg_f1": avg}

def _thropt_prepare_results(results):
   final = {}
   for partial in sorted(results, key = lambda x : -1 * x["threshold"]): 
      if "avg_f1" not in final or final["avg_f1"] < partial["avg_f1"]:
         final = partial
   info_msg("Best threshold {0} (F1: {1})".format(final["threshold"], final["avg_f1"]))
   return final

def _cfopt_neighbors(x):
   if x <= 0:
      return []
   current = x
   result = []
   for i in range(0, 2):
      step_below = 10 ** (ceil(log10(current))-1)
      current = x - step_below
      result.append(current)
   current = x
   for i in range(0, 2):
      step_above = 10 ** (ceil(log10(current+1))-1)
      current = current + step_above
      result.append(current)
   return result

def _cfopt_evaluate_in(point, outdir, kernel_type, folds):
   outdir = join(outdir, "{0}-cf{1}".format(kernel_type, point))
   scores = []
   for train,test in folds.items():
      model = learn(outdir, train, kernel_type = kernel_type, cost_factor = point)
      predictions = classify(test, model, outdir = outdir)
      result = evaluate(predictions, test)
      scores.append(result["f1"])
   avgscore = sum(scores) / len(scores) 
   return avgscore

class _Optimizer(object):
   
   def __init__(self, evaluate_in_func, neighbors_func, evaluate_in_args = {}, initial_point = 5, ncpu = 1):
      self.scores = { 0 : 0}
      self.initial_point = initial_point
      self.evaluate_in_func= evaluate_in_func
      self.evaluate_in_args= evaluate_in_args
      self.neighbors_func = neighbors_func
      self.elapsed_time = 0
      self.ncpu = ncpu
      
   def optimize(self):
      currentNode = self.initial_point
      while True:
         points = self.neighbors_func(currentNode) + [currentNode]
         pool = workers_pool(self.ncpu)
         scheduled = []
         for x in points:
            if x not in self.scores:
               self.evaluate_in_args["point"] = x
               pool.submit_job(self.evaluate_in_func, (self.evaluate_in_args))
               scheduled.append(x)
         results = pool.get_results()
         self.elapsed_time += pool.get_elapsed_time()
         for point, result in zip(scheduled, results):
            self.scores[point] = result
         nextEval = None
         nextNode = None
         for point in self.scores:
            if nextEval == None or self.scores[point] > nextEval:
               nextNode = point
               nextEval = self.scores[point]
         if nextEval <= self.scores[currentNode]:
            # Return current node since no better neighbors exist
            return {"point": currentNode, 
                    "value": self.scores[currentNode],
                    "elapsed_time": self.elapsed_time}
         else:
            currentNode = nextNode

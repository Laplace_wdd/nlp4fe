from flink.utils import persistency
from flink import pytk
from flink.utils import fileutils, persistency
from flink.utils.logging import info_msg

from flink.pytk.defs.FragIdx cimport FragIdx
from flink.utils.tree cimport Node, parse

def _merge_dictionaries(inlist, outdict):
   cdef FragIdx idx
   if len(inlist) == 1:
      fileutils.move_file(inlist[0], outdict)
      return outdict
   idx = persistency.load(inlist[0])
   for to_add in inlist[1:]:
      idx.merge(persistency.load(to_add))
   persistency.save(idx, outdict)
   return outdict

def _learn_model(data, model = None, learn_options = {}):
   params = pytk.get_learn_parameters(**learn_options)
   if model is None:
      param_str = params.replace(" ", "").replace(".", ",")
      model = "{0}.model{1}".format(data, param_str)
   info_msg("Learning model {1} from {0} ({2})".format(data, model, params))
   pytk.learn(data, model, params)
   return model

def _linearize_file(infile, dictionary, outfile, use_gradient_components, kernel_params):
   cdef dict mapping, lookup_res
   cdef str label, treeSurface
   cdef Node node
   cdef double example_norm = 0, value
   cdef int real_nid
   cdef FragIdx index = persistency.load(dictionary)
   cdef object frag_module
   cdef object example
   if use_gradient_components:
      if kernel_params is None:
         raise RuntimeError("kernel_param argument must be set if "
                            "use_gradient_components is True")
      frag_module = pytk.get_frag_module(kernel_params.kernel_name)
   cdef object outf = fileutils.open_for_writing(outfile, "w")
   mapping = {}
   for example in pytk.parse_examples(infile, False):
      if isinstance(example, pytk.Example):
         label = example.label_string
         treeSurface = example.trees[0]
         node = parse(treeSurface)
         lookup_res = index.lookup(node, True)

         outf.write(label)
         
         if use_gradient_components:
            example_norm = pytk.norm(treeSurface, kernel_params)
         
         for feat_id in sorted(lookup_res.keys()):
            data = lookup_res[feat_id]
            
            # handle virtual feature mapping
            if "real_nid" in data:
               real_nid = data["real_nid"]
               if real_nid not in mapping:
                  mapping[real_nid] = set()
               mapping[real_nid].add(feat_id)
               #print real_nid
               del data["real_nid"]
               
            value = data["occurrencies"]
            if use_gradient_components: 
               del data["occurrencies"]
               value *= frag_module.get_gradient_component(
                  kernel_params, example_norm, **data)
            outf.write(" " + str(feat_id) + ":" + str(value))
         if len(lookup_res) == 0:
            outf.write(" 1:1") #dummy feature for empty record!
         outf.write("\n")
      else:
         outf.write(example)
   outf.close()
   mapping_file = outfile + ".nid_map"
   persistency.save(mapping, mapping_file)
   return (outfile, mapping_file)

def _classify(datafile, model):
   predictions = datafile + ".pred"
   pytk.classify(datafile, model, predictions)
   return predictions


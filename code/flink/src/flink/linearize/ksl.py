#!/usr/bin/env/python3
"""
Learning models in the tree kernel space: :mod:`flink.linearize.ksl`
--------------------------------------------------------------------

This executable module implements the KSL stage of the linearization
process.

Available command line options can be listed by invoking the module
with the ``-h`` flag::

   python3 -m flink.linearize.ksl -h

In the simplest usage scenario, we want to learn a TK model from some
valid SVM-Light-TK training file ``<training>``, using the kernel
``<kernel_type>`` (correct values are ``STK`` for the syntactic tree kernel
and ``PTK`` for the partial tree kernel) and store the results
in the output directory ``<outdir>``.

We can do so by issuing the command::

   python3 -m flink.linearize.ksl -o <outdir> -k <kernel_type> <training>
   
Learning a model in this way is the first step of the *OPT* and *LIN*
architectures.

For the *Split* architecture, we can automatically partition the
training file into ``<num_splits>`` chunks and learn as many models by
adding the ``-s <num_splits>`` option::

   python3 -m flink.linearize.ksl -o <outdir> -k <kernel_type>
                                  -s <num_splits> <training>

.. note::

   Split learning is carried out by producing balanced splits of the
   training data file, i.e. each split will approximately contain the
   same positive/negative example ratio than the original training file.
   
   Still, in some cases each split could contain too few positive
   examples for the learning algorithm to generalize properly, resulting
   in errors that can affect all the subsequent activities of the
   linearization process.
   
   The responsibility of using a reasonable number of splits 
   (with respect to the data set and task at hand) is left to 
   the user of the module.

For each split ``<x>``, the program will produce the following output
files:

   - ``<outdir>/<x>``: the training data split
   - ``<outdir>/<x>-model<params>``: the corresponding model file, where
      ``<param>`` is the concatenation of parameters used to learn the
      model
   - ``<outdir>/<x>-model<params>.stdout``: the standard output of
     SVM-Light-TK
   - ``<outdir>/<x>-model<params>.stderr``: the standard error of
     SVM-Light-TK

In case of non-split learning, the only split is the actual training file,
which is copied to the output directory.
   
If running on a multi-processor or multi-core architecture, it makes
sense to use the ``-n <ncpu>`` option along with the ``-s <num_splits>`` 
option to use up to ``<ncpu>`` parallel processes for learning the models::

   python3 -m flink.linearize.ksl -o <outdir> -k <kernel_type>
                                  -s <num_splits> -n <ncpu> <training>
                                  
In both cases (single model or split model learning), we can use the
parameter ``-j <cost_factor>`` to force a cost factor different than 1::

   python3 -m flink.linearize.ksl -o <outdir>
                                  -k <kernel_type> -j <cost_factor>
                                  -s <num_splits> -n <ncpu> <training>

.. note::

   In the case of split learning, the same cost factor is used for
   all the models.
   
Lastly, the ``-c`` flag can be used to force :mod:`flink.linearize.ksl`
to erase the contents of ``<outdir>`` before learning the new models.
"""

from flink.utils.cli import cliparse, group, int_option, str_option,\
   config_properties_option, flag
from flink.utils.logging import result_msg, info_msg
from flink.activities import learn
from multiprocessing import cpu_count
from flink.utils import fileutils

def main(cmdline_args):

   #============================================================================
   # Parse command line arguments
   #============================================================================
      
   options,args = cliparse(
      group("Multiprocessing options"),
      int_option("n", "ncpu", help = "use up to NCPU processes (the number of " 
         "processes actually used is limited by the value of NUM_SPLITS)",
         default = cpu_count()), 
      group("Algorithm parameters"),
      int_option("s", "num_splits", help = "number of splits for KSL", default = 1),
      str_option("k", "kernel_type", help = "kernel type (STK or PTK)"),
      int_option("j", "cost_factor", help = "cost factor for TK learning", default = 1),
      group("Output options"),
      str_option("o", "outdir", help = "output directory"),
      flag("c", "clean_outdir", help = "delete output directory before running"),
      group("General options"),
      config_properties_option(),
      cli_num_args = 1,
      args = cmdline_args,
      usage = "%prog [options] <training data file>")
      
   train = args[0]
   
   if options.clean_outdir:
      info_msg("Cleaning output directory {0}".format(options.outdir))
      fileutils.rmdir(options.outdir)
   
   #============================================================================
   # Kernel space learning
   #============================================================================
   
   models, elapsed_time = learn(options.outdir, train, 
         num_splits = options.num_splits, 
         ncpu = options.ncpu, get_etime = True,
         kernel_type = options.kernel_type,
         cost_factor = options.cost_factor)
   
   if type(models) == str:
      models = [models]
   
   result_msg("Learned {0} model(s): {1}".format(len(models), " , ".join(models)))
   result_msg("Elapsed time: {0}".format(elapsed_time))

   return 0

if __name__ == "__main__":
   import sys
   main(sys.argv[1:])
   

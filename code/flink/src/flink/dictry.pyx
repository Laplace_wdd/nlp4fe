from flink.utils import fileutils, persistency
from flink.utils.logging import info_msg
from os.path import join, basename, dirname
from flink.private.activities import _linearize_file, _classify, \
   _learn_model, _merge_dictionaries
from flink.utils.pool import workers_pool
from flink import pytk
from flink.utils.mathutils import dense_vector
from flink.pytk.miners import greedy_miner
import re, sys

from flink.pytk.defs.FragIdx cimport FragIdx, IdxNode

def dict_mine(dictionary):
   cdef FragIdx fragidx = persistency.load(dictionary)
   reversedict = {}
   cdef IdxNode x
   for x in fragidx.generator():
         print x.nid +' '+ x.as_tree()

"""
This module implements facilities for the optimization of the cost
factor for model learning (:func:`optimize_cost_factor`) and of the
threshold parameter of the greedy mining algorithm 
(:func:`optimize_threshold`) for kernel space mining.

.. warning:: This activities can be extremely time consuming!
   
   They carry out several learning/classification iterations, and can
   take very long time for larger datasets.
"""

from os.path import join
from flink.utils import fileutils
from flink.private.optimize import _thropt_evaluate_in, \
   _thropt_prepare_results, _cfopt_evaluate_in, _cfopt_neighbors, _Optimizer,\
   _get_threshold_values
from flink.utils.pool import workers_pool


def optimize_threshold(outdir, models, benchmark, num_folds, ncpu, get_etime = False, lowerbound=2.5, upperbound = 1000):
   """
   Optimize the threshold parameter of the 
   :func:`~flink.pytk.miners.greedy_mining` algorithm.
   
   *models* is the set of models for which the miner must be optimized,
   and *benchmark* is the data file to be used as training/test 
   benchmark (so it should be either the same training file used to 
   learn the models *models* or an ad-hoc development set).
   
   The models listed in *models* are mined for different values of
   the threshold parameter. For each threshold value, the resulting 
   fragment dictionary is used to linearize the benchmark. 
   The linearized benchmark is used to create *num_folds* folds and
   to cross-validate the accuracy of the resulting linear classifier.
   The best threshold value is selected so as to maximize the average
   F1 measure across the folds.
   
   The arguments *lowerbound* and *upperbound* can be used to change
   the range of threshold values to be considered. The algorithm used
   to generate the set of threshold values is the following::
   
      oom = range(int(log10(lowerbound))-1, int(log10(upperbound))+1)
      result = []
      for order in oom:
         for factor in [2.5, 5, 7.5, 10]:
            value = (10**order) * factor
            if value > upperbound:
               return result
            if value >= lowerbound:
               result.append(value)
   
   *ncpu* is the number of parallel processes that will be activated.
   
   *outdir* is the directory where the function produces its output.
   For each threshold value, the function creates a distinct
   sub-directory in *outdir* where the relative files are stored.
   
   The function returns a dictionary with the following keys:
   
   - ``threshold``: the best threshold value
   - ``avg_f1``: the average cross-fold F1-measure for the given 
     threshold
   - ``dictionary``: the path to the fragment dictionary corresponding 
     to the best threshold
   - ``linearized_benchmark``: the path to the linearization of
     *benchmark* obtained for the best threshold
   - ``nid_mapping``: the path to the feature mapping resulting from
     the linearization of *benchmark*
     
   If *get_etime* is |True|, then the function returns a pair in which
   the first element is the aforementioned dictionary, and the second
   is the time required to perform the optimization.
   """
   pool = workers_pool(ncpu)
   for thr in _get_threshold_values(lowerbound, upperbound):
      thr_outdir = join(outdir, "mineropt_thr_{0}".format(thr))
      kwargs = {"threshold": thr,
                "outdir": thr_outdir, 
                "num_folds": num_folds,
                "models": models,
                "benchmark": benchmark}
      pool.submit_job(_thropt_evaluate_in, kwargs)
   if get_etime:
      return _thropt_prepare_results(pool.get_results()), pool.get_elapsed_time()
   return _thropt_prepare_results(pool.get_results())
      
def optimize_cost_factor(outdir, benchmark, kernel_type, num_folds, initial_point = 10, ncpu = 1):
   """
   Perform cross-fold evaluation on the datafile *benchmark* to optimize
   ``svm_learn`` cost factor.
   
   *kernel_type* is the kernel to use for learning the model (one of
   those allowed by :func:`flink.pytk.get_learn_parameters`), whereas
   *num_folds* is the number of folds for cross-fold evaluation.
   
   *outdir* is the directory where output files are produced, and *ncpu*
   is the maximum number of parallel processes to employ.
   
   The optimization is carried by means of a simple hill-climbing
   algorithm. The initial value for the optimization can be set via the
   *initial_point* parameter.
   
   The function returns a dictionary with three values indexed by the
   following keys:
   
   - ``point``: the estimated optimal cost_factor
   - ``value``: the cross-fold average F1-measure in that point
   - ``elapsed_time``: the time required to perform the optimization
   """
   outdir = join(outdir, "cfopt")
   folds = fileutils.make_folds(benchmark, num_folds, outdir)
   args = {"outdir": outdir, "kernel_type": kernel_type, "folds": folds}
   optimizer = _Optimizer(
      _cfopt_evaluate_in, _cfopt_neighbors, 
      evaluate_in_args = args, 
      initial_point = initial_point, 
      ncpu = ncpu)
   return optimizer.optimize()
   

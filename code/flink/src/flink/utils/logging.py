"""
***************************************************************
:mod:`flink.utils.logging` --- a very simple logging facility
***************************************************************

Display formatted log messages.

Messages are actually printed or not depending on the value of the
configuration property :data:`flink.config.logging_verbosity`.
"""

from datetime import datetime
from flink.utils.props import get_property
import sys

def _log_msg(level, text):
   if level <= get_property("logging_verbosity"):
      stamp = datetime.now()
      calling_func = sys._getframe(2).f_code.co_name
      msg = "[{0:5}] {1} -- {3} -- {2}".format("#" * (5-level), stamp, text, calling_func, stamp)
      print(msg)
      
def debug_msg(text):
   """
   Print a debug message.
   """
   _log_msg(5, text)
   
def info_msg(text):
   """
   Print an informative message.
   """
   _log_msg(4, text)
   
def result_msg(text):
   """
   Print some result or process output.
   """
   _log_msg(3, text)
   
def warning_msg(text):
   """
   Print a worning message.
   """
   _log_msg(2, text)
   
def error_msg(text, critical = False):
   """
   Print an error message. If *critical* is |True|, a :class:`RuntimeError`
   is raised.
   """
   _log_msg(1, text)
   if critical:
      raise RuntimeError(text)


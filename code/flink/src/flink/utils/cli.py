"""
***************************************************************
:mod:`flink.utils.cli` --- command line parsing facilities
***************************************************************

A wrapper around :class:`optparse.OptionParser` to easily build
command line parsers.

The module provides a higher level interface for command line
parsing, implementing mandatory options and making easier to
create groups of command line options.

The user invokes the :func:`cliparse` function, passing as
arguments a variable number of :class:`cliopt` objects in the
order in which they should appear in the help message.

On success (all the required arguments and options were present)
the function returns the same ``(options, args)`` pair as
:class:`optparse:OptionParser`.

A set of high-level factory functions (:func:`str_option`,
:func:`int_option`, :func:`float_option`, :func:`flag`,
:func:`list_option`, :func:`config_properties_option`) are
used to ease the creation of typed options.

A special class, :class:`group`, is used to easily create
option groups. All the options added after creating a group
are automatically included in that group. When a new group
is added, all subsequent options will be added to the last
group.

As an example, the following instructions::

   from flink.utils.cli import cliparse, int_option, \\
      float_option, group, str_option

   options,args = cliparse(
      
      group("First group"),
      
      int_option("a", "option_a", 
         help = "This option requires an integer argument", 
         default = 1),
         
      float_option("b", "option_b",
         help = "This non-mandatory option requires a float argument",
         mandatory = False),
      
      group("Second group"),
      
      str_option("c", "option_c", 
         help = "A mandatory string option for my parser"),
         
      cli_num_args = 2, # there must be exactly 2 positional arguments
      usage = "%prog [options] <1st required arg> <2nd required arg>")
      
would produce the following help message::

   Usage: test.py [options] <1st required arg> <2nd required arg>
   
   Options:
     -h, --help            show this help message and exit
   
     First group:
       -a OPTION_A, --option_a=OPTION_A
                           This option requires an integer argument [default:1]
       -b OPTION_B, --option_b=OPTION_B
                           This non-mandatory option requires a float argument
   
     Second group:
       -c OPTION_C, --option_c=OPTION_C
                           A mandatory string option for my parser  (*)
   
   Options marked with (*) are mandatory.
   
.. seealso::
   The command line parsers in :mod:`flink.linearize.ksl` and 
   :mod:`flink.linearize.binary` for richer examples.
"""

from optparse import OptionParser, Option, OptionGroup
from flink.utils import props
import sys

class cliopt(Option):
   """
   A wrapper class around :class:`optparse.Option` to build command line 
   options.
   
   - *short* is a one-character string to be used as short flag for the
     option.
   
     .. note::
      
        Unlike :class:`optparse.OptionParser`, the trailing ``-`` must
        not be included!
   
   - *dest* is the name of the attribute where the option value will be
     stored to, and is also used for the long name of the command line flag.
   
   - If *mandatory* is |True|, then the user of the program will be required
     to provide a value for the option, unless a default is provided by the
     programmer (via ``option_args["default"]``).
     
   - If *group* is not |None|, then the option is added to the group titled
     *group*.
     
   - *option_args* is a set of keyword arguments that will be forwarded to
     the constructor of :class:`optparse.Option`. 
     
   .. note::

      Users should not use :class:`cliopt` directly, instead they should
      use one of the higher level facilities like :func:`str_option`,
      :func:`int_option`, depending on the kind of option they want to
      create.
   """
   def __init__(self, short, dest, mandatory = True, group = None, **option_args):
      self._mandatory = mandatory
      self._group = group
      if "default" not in option_args:
         option_args["default"] = None
      if "action" not in option_args:
         option_args["action"] = "store"
      help = ""
      if option_args["default"] is not None:
         help += "[default:{0}]".format(option_args["default"])
      if self._mandatory and option_args["default"] is None:
         help += " (*)"
      if "help" not in option_args: option_args["help"] = help
      else: option_args["help"] += " " + help
      option_args["dest"] = dest
      Option.__init__(self,  "-" + short, "--" + dest,  **option_args)
      
def config_properties_option(short = "D",  
      dest = "config_property", **kwargs):
   """
   Add an option to override the values of configuration properties.
   
   .. seealso::
      :mod:`flink.config` and :mod:`flink.utils.props`.
   
   The arguments *short* and *dest* are the same as for :class:`cliopt`.
   
   Adding this option to the command line parser, allows the user of
   a program to override the values of one or more configuration
   properties by using a syntax like::
   
      -D first_prop_name=value1 -D second_prop_name=value2
      
   (assuming that ``D`` was used as short flag for the option).
   
   When parsing the command line, the parser will also check the
   consistency of the user's assignments, verifying that the required
   properties exist and that the values they are given are compatible
   with each property's type.
   """
   if "help" not in kwargs:
      kwargs["help"] = "assign a value to a configuration property. " \
         "The option can be repeated several times to set multiple " \
         "properties. Values passed to this " \
         "option must be in the form <name>=<value>. Configuration " \
         "options are listed in the module flink.config"
   kwargs["action"] = "callback"
   kwargs["nargs"] = 1
   kwargs["type"] = "string"
   kwargs["callback"] = _handle_property_opt
   return cliopt(short, dest, mandatory = False, **kwargs)
      
def list_option(short, dest, **kwargs):
   """
   Create an option for which users can specify multiple values,
   using a syntax like::
   
      -x first_value -x second_value
      
   assuming that *short* is set to ``x``.
   
   The arguments *short* and *dest* are the same as for :class:`cliopt`.
   """
   kwargs["action"] = "append"
   return cliopt(short, dest, mandatory = False, **kwargs)
      
def int_option(short, dest, **kwargs):
   """
   Creating a command line option to store an integer parameter.
   
   The arguments *short* and *dest* are the same as for :class:`cliopt`.
   """
   kwargs["type"] = "int"
   return cliopt(short, dest, **kwargs)

def float_option(short, dest, **kwargs):
   """
   Create a command line option to store a floating point parameter.
   
   The arguments *short* and *dest* are the same as for :class:`cliopt`.
   """
   kwargs["type"] = "float"
   return cliopt(short, dest, **kwargs)

def str_option(short, dest, **kwargs):
   """
   Create a command line option to store a string valued parameter.
   
   The arguments *short* and *dest* are the same as for :class:`cliopt`.
   """
   kwargs["type"] = "str"
   return cliopt(short, dest, **kwargs)

def flag(short, dest, **kwargs):
   """
   Create a boolean flag. 
   
   By default, the value of the flag is set to |False|. 
   If the user adds the flag to the command line, then its value is set 
   to |True|.
   
   The arguments *short* and *dest* are the same as for :class:`cliopt`.
   """
   kwargs["action"] = "store_true"
   kwargs["mandatory"] = False
   return cliopt(short, dest, **kwargs)
      
class group:
   """
   Create a group of command line options.
   
   *title* is the title for the group of options.
   """
   def __init__(self, title):
      self.title = title
      
def _check_num_args(parser, cli_num_args, args):
   if cli_num_args is not None:
      cli_num_args = str(cli_num_args)
      if cli_num_args.endswith("+"):
         num = int(cli_num_args[:-1])
         if len(args) < num:
            parser.error("the program requires at least {0} positional arguments".format(num))
      elif cli_num_args.endswith("-"):
         num = int(cli_num_args[:-1])
         if len(args) > num:
            parser.error("the program accepts at most {0} positional arguments".format(num))
      else:
         num = int(cli_num_args)
         if len(args) != num:
            parser.error("the program requires exactly {0} positional arguments".format(num))
            
def _check_mandatory_opts(parser, mandatory, options):
   for mand_opt in mandatory:
      if options.__dict__[mand_opt] is None:
         parser.error("mandatory option not specified: {0}".format(mand_opt))
      
def cliparse(*options, cli_num_args = None, args=sys.argv[1:], **optparser_options):
   parser = OptionParser(**optparser_options)
   parser.epilog = "Options marked with (*) are mandatory."
   mandatory = set()
   current_group = None
   for option in options:
      if type(option) == group:
         if current_group is not None:
            parser.add_option_group(current_group)
         current_group = OptionGroup(parser, option.title)
      elif type(option) == cliopt:
         if option._mandatory:
            mandatory.add(option.dest)
         if current_group is not None:
            current_group.add_option(option)
         else:
            parser.add_option(option)
   if current_group is not None:
      parser.add_option_group(current_group)
   options,args  = parser.parse_args(args=args)
   _check_num_args(parser, cli_num_args, args)
   _check_mandatory_opts(parser, mandatory, options)
   return options, args

def _handle_property_opt(option, opt, value, parser):
   if value is None or value.find("=") == -1:
      parser.error("Configuration properties must be in the form <name>=<value>")
   (prop_name, prop_value) = value.split("=", 1)
   try:
      props.set_property(prop_name, prop_value)
   except props.NoSuchProperty:
      parser.error("No such configuration property: {0}".format(prop_name))
   except props.IncompatiblePropertyValue as exc:
      parser.error("Type of property {0} ({1}) is not compatible " \
         "with type configuration option {2} ({3})".format(
            prop_value, exc.received_type, prop_name, exc.expected_type))
      

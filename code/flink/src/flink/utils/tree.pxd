
cdef class Node:
   cdef public str label
   cdef public Node parent
   cdef public list children
   cdef public int child_offset

   cdef public Node getParent(self)
   cdef public Node getRoot(self)
   cdef public int isLeaf(self)
   cdef public int isPreterminal(self)
   cdef public list nodesIterator(self, int includeLeaves=*)

cdef public Node parse(str data)


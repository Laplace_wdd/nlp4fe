"""
********************************************************
:mod:`flink.utils.tree` --- manage parenthesized trees
********************************************************

This module provides facilities for handling trees.

It allows to parse strings representing trees in parenthesized
notation and to manipulate the resulting data structures.  
"""

import re

patt1 = re.compile(r"\s+([^ )(]+)\)")
patt2 = re.compile(r"\s+")

cdef Node parse(str data):
   """
   Parse a string and return a :class:`Node` object corresponding
   to the root of the tree.
   
   The input string *data* must contain a parenthesized representation
   of a tree.
   """ 
   cdef Node node, root = None
   cdef str x, lab
   cdef list labels
   data = re.sub(patt1, r"(\1))", data)
   data = re.sub(patt2, "", data)
   cdef list split = [x for x in data.split("(") if len(x) > 0]
   for x in split:
      labels = x.split(")")
      for lab in labels:
         if len(lab) > 0:
            node = Node(lab, root)
            root = node
         else:
            if root.getParent() is None:
               return root
            root = root.getParent()
   return root

cdef class Node:
   """
   A class to handle rooted ordered trees.
   
   Each node in a tree is a tree itself.
   
   *label* is the label of the node, and *parent* a reference
   to the parent node. If *parent* is |None|, then the node is
   the root of a tree.
   
   Public fields:
   
   - *label*: the label of the node
   - *parent*: reference to the parent node (|None| if the
     node is the root of the tree)
   - *children*: an ordered sequence of the children of this
     node. It is empty if the node is a leaf
   - *child_offset*: if the node is not the root of a tree,
     it is the relative offset of the node with respect to
     the parent node (i.e. ``0`` if it is the first child,
     ``1`` if it is the second, and so on). 
   """
   
   def __init__(self, str label, Node parent = None):
      self.label = label
      self.parent = parent
      self.children = []
      if parent == None:
         self.child_offset = -1
      else:
         self.child_offset = len(parent.children)
         parent.children.append(self)
         
   cdef list nodesIterator(self, int includeLeaves = 0):
      """
      Generator of all the nodes dominated by this node.
      
      The first yielded element is this node itself, then
      its descendants are yielded in depth-first order.
      
      If *includeLeaves* is |True|, then also leaf nodes
      are yielded, otherwise the generator will stop at
      pre-terminal level.
      """
      cdef list result = []
      cdef Node x, y
      result.append(self)
      for x in self.children:
         if includeLeaves or not x.isLeaf():
            for y in x.nodesIterator(includeLeaves):
               result.append(y)
      return result
               
   cdef public int isLeaf(self):
      """
      Return |True| if the node is a leaf, |False| otherwise.
      """
      return len(self.children) == 0
   
   cdef public int isPreterminal(self):
      """
      Return |True| if the node is pre-terminal (i.e. all its
      children are leaves), |False| otherwise.
      """
      return len(self.children) == 1 and self.children[0].isLeaf()
   
   cdef public Node getRoot(self):
      """
      Return a reference to the root node of the tree this nodes
      belong to.
      """
      if self.parent == None:
         return self
      return self.parent.getRoot()
     
   cdef public Node getParent(self):
      """
      Return a reference to the parent node of this node.
      """
      return self.parent
   
   def __str__(self):
      """
      Return a parenthesized representation of the (sub-)tree rooted in this
      node.
      """
      if self.isLeaf():
         return "(%s)" % self.label 
      else: 
         contents = "".join([str(child) for child in self.children])
         return "(%s%s)" % (self.label, contents)
      

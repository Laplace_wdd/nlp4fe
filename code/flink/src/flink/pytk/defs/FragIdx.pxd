from flink.pytk.defs.RelevanceData cimport RelevanceData
from flink.pytk.defs.Fragment cimport Fragment
from flink.utils.tree cimport Node

cdef list fragment_indexing_iterator(Fragment fragment)

cdef class IdxNode(dict):
   cdef public tuple label
   cdef public IdxNode parent
   cdef public int nid
   cdef public RelevanceData data

   cdef public list backtrace(self)
   cdef public str as_tree(self)

cdef class FragIdx:
   cdef public IdxNode frags
   cdef public int next_nid
   cdef public int num_leaves

   cdef public IdxNode put(self, list labs, RelevanceData data)
   cdef public IdxNode putFragment(self, Fragment frag)
   cdef public dict lookup(self, Node node, int recursive=*)
   cdef public void merge(self, FragIdx other)
   cdef public IdxNode put_node(self, IdxNode idxnode)
   cdef public list generator(self, IdxNode current=*, list result=*)
   cdef void _merge(self, IdxNode scur, IdxNode ocur)
   cdef IdxNode _add_node(self, tuple label, IdxNode parent)
   cdef public dict filter(self, int min_freq, double min_relevance, FragIdx result_idx, IdxNode current=*, dict nid_map=*)


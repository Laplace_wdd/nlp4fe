cdef class RelevanceData:

   def __init__(self, float relevance = 0, int pos_count = 0, int neg_count = 0):
      self.relevance = relevance
      self.pos_count = pos_count
      self.neg_count = neg_count
      if (relevance > 0 and pos_count == 0):
         self.pos_count += 1
      elif (relevance < 0 and neg_count == 0):
         self.neg_count += 1
         
   def __reduce__(self):
      return (RelevanceData, (self.relevance, self.pos_count, self.neg_count))

   def __richcmp__(self, RelevanceData other, int op):
      if op == 0: # <
         return abs(self.relevance) < abs(other.relevance)
      elif op == 2: # ==
         return abs(self.relevance) == abs(other.relevance)
      elif op == 4: # >
         return abs(self.relevance) > abs(other.relevance)
      elif op == 5: # >=
         return abs(self.relevance) >= abs(other.relevance)
      elif op == 1: # <=
         return abs(self.relevance) <= abs(other.relevance)
      elif op == 2: # !=
         return abs(self.relevance) != abs(other.relevance)
         
   def __str__(self):
      return "[{0.relevance} +{0.pos_count} -{0.neg_count}]".format(self)

   #def __lt__(self, other):
   #   return abs(self.relevance) < abs(other.relevance)
   #
   #def __eq__(self, other):
   #   return abs(self.relevance) == abs(other.relevance)
   #
   #def __gt__(self, other):
   #   return abs(self.relevance) > abs(other.relevance)
   #
   #def __ge__(self, other):
   #   return abs(self.relevance) >= abs(other.relevance)
   
   cdef public void update(self, RelevanceData data):
      self.relevance += data.relevance
      self.pos_count += data.pos_count
      self.neg_count += data.neg_count

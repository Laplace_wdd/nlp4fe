from flink.pytk.defs.RelevanceData cimport RelevanceData
from flink.pytk.defs.Fragment cimport Fragment
from flink.utils.tree cimport Node
import sys

def __newobj__(cls, *args):
   return cls.__new__(cls, *args)

cdef list fragment_indexing_iterator(Fragment fragment):
   cdef list result, history, temp
   cdef Fragment first, current
   result = []
   history = fragment.history()
   first = history.pop()
   result.append(tuple([x.label for x in first.expanded_nodes]))
   current = first
   while current != None:
      if len(current.expanded_offsets) > 0:
         result.append(current.expanded_offsets)
      temp = []
      for x in current.productions:
         if x is None:
            temp.append(None)
         else:
            temp.append(x.label)
      if len(temp) > 0:
         result.append(tuple(temp))
      if len(history) > 0:
         current = history.pop()
      else:
         current = None
   return result

cdef class IdxNode(dict):
   def __init__(self, tuple label = None, IdxNode parent = None, int nid = 0, RelevanceData data = None):
      self.label = label
      self.parent = parent
      self.nid = nid
      self.data = data

   cdef public list backtrace(self):
      result = [self]
      current = self
      while current.parent and current.parent.label:
         result.append(current.parent)
         current = current.parent
      return result

   def __str__(self):
      data = "data:{0}".format(self.data) if self.data else ""
      return "nid:{0.nid} {1}".format(self, data)

   def __reduce__(self):
      props = {}
      props["label"] = self.label
      props["parent"] = self.parent
      props["nid"] = self.nid
      props["data"] = self.data
      props["DICT"] = dict(self)
      return (__newobj__, (self.__class__,), props)

   def __setstate__(self, props):
      self.update(props["DICT"])
      self.nid = props["nid"]
      self.data = props["data"]
      self.label = props["label"]
      self.parent = props["parent"]

   cdef public str as_tree(self):
      trace = [x.label for x in self.backtrace()]
      current = []
      parent_offs = []
      next = []
      state_labs = True
      root = None
      while len(trace) > 0:
         elem = trace.pop()
         if state_labs:
            poff = 0
            for lab in elem:
               if lab == None:
                  poff += 1
                  next.append(None)
               else:
                  if len(parent_offs) > 0:
                     tempoff = parent_offs[poff]
                     if isinstance(tempoff,tuple):
                        tempoff = tempoff[0]
                     parent = current[tempoff]
                  else:
                     parent = None
                  node = Node(lab, parent)
                  next.append(node)
                  if root == None:
                     root = node
            current = next
            next = []
         else:
            parent_offs = elem
         state_labs = not state_labs
      return str(root)


cdef class FragIdx:

   def __init__(self):
      self.frags = IdxNode()
      self.next_nid = 2
      self.num_leaves = 0

   def __reduce__(self):
      props = {}
      props["frags"] = self.frags
      props["next_nid"] = self.next_nid
      props["num_leaves"] = self.num_leaves
      return (__newobj__, (self.__class__,), props)

   cdef list generator(self, IdxNode current = None, list result = None):
      if current is None:
         current = self.frags
      if result is None:
         result = []
      if current.data:
         result.append(current)
      cdef tuple k
      for k in current:
         self.generator(current[k], result)
      return result

   def size(self):
      return self.next_nid - 2

   def __setstate__(self, props):
      self.frags = props["frags"]
      self.next_nid = props["next_nid"]
      self.num_leaves = props["num_leaves"]

   cdef public IdxNode put(self, list labs, RelevanceData data):
      cdef IdxNode current = self.frags
      cdef tuple x
      for x in labs:
         if x not in current:
            current = self._add_node(x, current)
            #current[x] = IdxNode(x, current, self.next_nid)
            #self.next_nid += 1
         else:
            current = current[x]
      if not current.data:
         current.data = data
         self.num_leaves += 1
      else:
         current.data.update(data)
         #BAD BAD BAD for debug purposes
         #pass
      return current

   cdef IdxNode _add_node(self, tuple label, IdxNode parent):
      newnode = IdxNode(label, parent, self.next_nid)
      parent[label] = newnode
      self.next_nid += 1
      return newnode

   cdef public IdxNode put_node(self, IdxNode idxnode):
      cdef RelevanceData data = idxnode.data
      cdef list trace = idxnode.backtrace()
      cdef IdxNode x
      trace.reverse()
      return self.put([x.label for x in trace], data)

   cdef public void merge(self, FragIdx other):
      self._merge(self.frags, other.frags)

   cdef void _merge(self, IdxNode scur, IdxNode ocur):
      cdef tuple key
      cdef IdxNode next_node
      if ocur.data is not None:
         if scur.data is None:
            scur.data = ocur.data
            self.num_leaves += 1
         else:
            scur.data.update(ocur.data)
      for key in ocur:
         if key not in scur:
            next_node = self._add_node(key, scur)
         else:
            next_node = scur[key]
         self._merge(next_node, ocur[key])

   def dump(self, current = None, level = 0):
      if current is None:
         current = self.frags
      for k,v in current.items():
         sys.stdout.write(" " * level)
         sys.stdout.write(str(k))
         sys.stdout.write(" ")
         sys.stdout.write(str(v))
         sys.stdout.write("\n")
         self.dump(v, level+1)
      if (level == 0):
         sys.stdout.write("distinct fragments: {0}\n".format(self.num_leaves))

   def get_min_max(self, IdxNode current = None, double cur_min = 100000, double cur_max = 0):
      if current is None:
         current = self.frags
      for key in current:
         (cur_min, cur_max) = self.get_min_max(current[key], cur_min, cur_max)
      if current.data:
         cur_min = min([cur_min, abs(current.data.relevance)])
         cur_max = max([cur_max, abs(current.data.relevance)])
      return (cur_min, cur_max)

   cdef public IdxNode putFragment(self, Fragment frag):
      return self.put(fragment_indexing_iterator(frag), 
            RelevanceData(frag.get_relevance()))

   cdef dict filter(self, int min_freq, double min_relevance, 
         FragIdx result_idx, IdxNode current = None, dict nid_map = None):
      cdef int leaves_before
      cdef IdxNode added_node
      cdef tuple key
      if current is None:
         current = self.frags
      if nid_map is None:
         nid_map = {}
      if current.data is not None:
         if current.data.pos_count + current.data.neg_count >= min_freq \
               and abs(current.data.relevance) >= min_relevance:
            leaves_before = result_idx.num_leaves
            added_node = result_idx.put_node(current)
            if result_idx.num_leaves > leaves_before:
               nid_map[current.nid] = added_node.nid
      for key in current:
         self.filter(min_freq, min_relevance, result_idx, current[key], nid_map)
      return nid_map

   # Abstract methods
   
   cdef dict lookup(self, Node node, int recursive=True):
      pass


from itertools import combinations

cdef class Fragment:
   def __init__(self, double example_weight, double example_norm, object kernel_params):
      self.parent_frag = None
      self.expanded_offsets = tuple()
      self.expanded_nodes = tuple()
      self.toExpand = []
      self.example_weight = example_weight
      self.example_norm = example_norm
      self.kernel_params = kernel_params

   cdef public int maxPossibleExpansions(self):
      return len(self.toExpand)

   cdef public list expansions(self, int expansion_cardinality):
      cdef list result = []
      cdef Fragment nextfrag
      if (len(self.toExpand) == 0 or 
            expansion_cardinality > len(self.toExpand) or
            expansion_cardinality < 1):
         return result
      cdef list l = [x for x in range(0, len(self.toExpand))]
      for com in combinations(l, expansion_cardinality):
         nextfrag = self._expand(com)
         if nextfrag is not None:
            result.append(nextfrag)
      return result
      
   cdef public int get_times_expanded(self):
      cdef int result = 1
      cdef Fragment current = self
      while current.parent_frag is not None:
         result += 1
         current = current.parent_frag
      return result
      
   cdef public list history(self):
      cdef list result = []
      cdef Fragment current = self
      while current is not None:
         result.append(current)
         current = current.parent_frag
      return result

   def __str__(self):
      res = ""
      for x in self.history():
         if x is None:
            res += " None"
         else:
            res += " (o:" + str(x.expanded_offsets) + " n:" + str([y.label for y in x.expanded_nodes]) + ")"
      return res

   # Abstract methods

   cdef public Fragment _expand(self, tuple offsets):
      pass

   cdef public int init_base_frag(self, Node node):
      pass

   cdef public double get_relevance(self):
      pass

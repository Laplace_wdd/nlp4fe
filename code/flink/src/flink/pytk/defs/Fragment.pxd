from flink.utils.tree cimport Node

cdef class Fragment:
   cdef public Fragment parent_frag
   cdef public tuple expanded_offsets
   cdef public tuple expanded_nodes
   cdef public list toExpand
   cdef public double example_weight
   cdef public double example_norm
   cdef public object kernel_params

   cdef public int init_base_frag(Fragment self, Node node)
   cdef public double get_relevance(Fragment self)
   cdef public list expansions(Fragment self, int num)
   cdef public int maxPossibleExpansions(Fragment self)
   cdef public int get_times_expanded(Fragment self)
   cdef public list history(Fragment self)
   cdef public Fragment _expand(self, tuple offsets)

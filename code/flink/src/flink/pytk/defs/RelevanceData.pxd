cdef class RelevanceData:
   cdef public float relevance
   cdef public int pos_count
   cdef public int neg_count

   cdef public void update(RelevanceData, RelevanceData)

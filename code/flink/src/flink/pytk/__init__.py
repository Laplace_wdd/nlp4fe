"""
The :mod:`flink.pytk` module provides a python wrapper to the
`SVM-Light-TK <http://disi.unitn.it/moschitti/Tree-Kernel.htm>`_ tool 
by `Alessandro Moschitti <http://disi.unitn.it/moschitti/>`_.

The module provides high level classes to handle SVM-Light-TK models
directly from Python, as well as low level functions to evaluate
tree kernel products, calculate the norm of a TK example, and more.

For learning and classification, the module provides two alternative
interfaces: either a direct call to the corresponding functions in 
SVM-Light-TK C layer, or the execution of the ``svm_learn`` and
``svm_classify`` programs in a separate process.

The former option requires less resources and should be faster, but
since there are still some glitches in the implementation, the 
default solution is the latter. This behaviour can be changed
by setting the value of the configuration property ``pytk_use_native``
(see :mod:`flink.config` and :mod:`flink.utils.props`) to |True|.

The module raises a :class:`RuntimeError` when loading if a native 
library for the present architecture is not found.
"""

from math import sqrt
from os.path import abspath, join, basename, dirname
import re, sys
from subprocess import Popen
from flink.utils import props

try:
   from platform import machine
   from ctypes import CDLL, c_char_p, c_long, c_double, c_short, c_int
   _native = CDLL(join(dirname(__file__), "native", "pytk.so"))
   _native.TK.restype = c_double
   _native.TK.argtypes = (c_char_p, c_char_p, c_long, c_double, c_double, c_short)
   _native.learn.restype = c_int
   _native.learn.argtypes = (c_char_p, c_char_p, c_char_p, c_char_p, c_char_p)
   _native.classify.restype = c_int
   _native.classify.argtypes = (c_char_p, c_char_p, c_char_p, c_char_p, c_char_p)
   _native.getdef_MAX_CHILDREN.restype = c_int
except:
   _native = None
   raise RuntimeError("Couldn't load native library for architecture {0}".format(machine()))

CONST_MAX_CHILDREN = _native.getdef_MAX_CHILDREN()
   
class ModelStats(object):
   def __init__(self, modelObj):
      self.num_vectors = 0
      self.num_positive_vectors = 0
      for x in modelObj.support_vectors():
         if x.label > 0:
            self.num_positive_vectors += 1
         self.num_vectors += 1
   
class Example(object):
   """Very general class to handle SVM-Light-TK examples.
   
   The constructor parameter *line* is an example line from an 
   SVM-Light-TK datafile or model.
   
   The example string is parsed into the following public fields: 
   
   - *label_string*: the label of the example (it is not parsed as a double
     value to allow the class to handle compact multi-class files in which
     labels are actually class labels instead of numeric values);
     
   - *trees*: the list of trees found in the example, in order of
     appearance;
   
   - *vectors*: the list of vectors found in the example, in order of
     appearance.
     
   *trees* and *vectors* are just lists of strings, no further processing
   of the input data is carried out.
   """
   def __init__(self, line):
      fields = [x.strip() for x in re.split("\s+(\|[BE][TV]\|)\s+", line)]
      
      self.label_string = fields[0]
      self.trees = []
      self.vectors = []
      
      index = 1
      while index < len(fields):
         if fields[index] == "|BT|":
            self.trees.append(fields[index+1])
         else:
            self.vectors.append(fields[index+1])
         index += 2
         
class Model:
   """Handler class for SVM-Light-TK models.
   
   *path_to_model* is the path to a valid SVM-Light-TK model file.
   """
   def __init__(self, path_to_model):
      self._model = path_to_model
      self._kernel_params = None
      
   def get_kernel_params(self):
      """Return an instance of :class:`KernelParams` with the
      relevant parameters scanned from the model.
      """
      if not self._kernel_params:
         self._kernel_params = parse_parameters(self._model)
      return self._kernel_params
   
   def get_factories(self):
      """Return the pair of classes to be used to handle,
      respectively, fragment generation and fragment indexing
      for the kernel type used to learn the model.
      """
      kernel_name = self.get_kernel_params().kernel_name
      return get_factories(kernel_name)

   def get_frag_module(self):
      """Return the pair of classes to be used to handle,
      respectively, fragment generation and fragment indexing
      for the kernel type used to learn the model.
      """
      kernel_name = self.get_kernel_params().kernel_name
      return get_frag_module(kernel_name)
   
   def support_vectors(self):
      """Scan a model, and for each support vector return a pair
      *(x,y)*, where *x* is the label of the support vector and
      *y* is the associated data.
      """
      result = []
      for example in parse_examples(self._model, True):
         result.append((example.label_string, example.trees[0]))
      return result
         
   def get_example_norm(self, data):
      """Return the norm of the tree described by *data* according
      to the parameters of the model."""
      return norm(data, self.get_kernel_params())

class KernelParams(object):
   """
   Facility class to handle the most relevant parameters
   of an SVM-Light-TK model.
   
   Public fields:
   
   - *kernel_name*: name of the kernel function used to learn a model
     (supported kernels: *linear*, *poly*, *STK* and *PTK*);
   - *decay_lambda*: value of the "lambda" decay factor;
   - *decay_mu*: value of the "mu" decay factor;
   - *normalize*: |True| if the kernel is normalized, |False| otherwise;
   - *poly_degree*: degree of the polynomial kernel.
   """
   
   def __init__(self):
      self.decay_lambda = None
      self.decay_mu = None
      self.normalize = None
      self.kernel_name = None
      self.poly_degree = None
   
   def __str__(self):
      return "Kernel Parameters: type={0} lambda={1} mu={2} normalize={3} poly_degree={4}".format(
            self.kernel_name, self.decay_lambda, self.decay_mu, self.normalize, self.poly_degree)
   
def get_learn_parameters(kernel_type = "linear", cost_factor = 1, decay_mu = 0.4, poly_degree = 1, decay_lambda = 0.4, normalize = True):
   """
   Generate command line flags for ``svm_learn`` according to the
   given parameters.
   
   *kernel_type* is a the name of the kernel to use. Accepted values
   are: ``STK`` (for the Syntactic Tree Kernel), ``PTK`` (for the 
   Partial Tree Kernel), ``poly`` for a polynomial kernel and 
   ``linear`` for the linear kernel.
   
   *cost_factor* is the cost factor by which training errors on
   positive examples outweigh errors on negative examples.
   
   *decay_lambda* and *decay_mu* are the decay factors for STK
   (lambda) and PTK (both lambda and mu) learning.
   
   *poly_degree* is the degree of the polynomial kernel.
   
   If *normalize* is set to :keyword:`True`, then kernel output is
   normalized between 0 and 1.
   
   The function returns a string with the appropriate flags to be
   passed to :func:`flink.pytk.learn`.
   """
   if kernel_type == "linear":
      options = "-t 0"
   elif kernel_type == "poly":
      options = "-t 1 -d {0}".format(poly_degree)
   elif kernel_type == "STK":
      options = "-t 5 -F 1 -L {0}".format(decay_lambda)
   elif kernel_type == "PTK":
      options = "-t 5 -F 3 -L {0} -M {1}".format(decay_lambda, decay_mu)
   else:
      raise RuntimeError("No learning parameters defined for kernel '{0}'".format(kernel_type))
   normalize = 1 if normalize else 0
   options += " -j {0} -N {1}".format(cost_factor, normalize)
   return options
      
def parse_parameters(modelfile):
   """
   Parse model parameters from the model identified by the path
   *modelfile* and return a :class:KernelParams object.
   """
   params = KernelParams()
   with open(modelfile) as model:
      for line in model:
         if line.find("each following line is a SV") != -1:
            break
         fields = line.split()
         if line.endswith("# kernel type"):
            type = int(fields[0])
            if type == 0:
               params.kernel_name = "linear"
            elif type == 1:
               params.kernel_name = "poly"
            elif type != 5:
               raise RuntimeError("Only linear, polynomial and tree kernels are supported")
         else:
            if len(fields) != 5:
               continue # not relevant lines
         if fields[4] == "-d":
            params.poly_degree = int(fields[0])
         elif fields[4] == "-N":
            normalize = int(fields[0])
            if normalize == 1 or normalize == 3:
               params.normalize = True
            else:
               params.normalize = False
         elif fields[4] == "-L":
            params.decay_lambda = float(fields[0])
         elif fields[4] == "-M":
            params.decay_mu = float(fields[0])
         elif fields[4] == "-F":
            params.kernel = int(fields[0])
            if params.kernel == 1:
               params.kernel_name = "STK"
            elif params.kernel == 3:
               params.kernel_name = "PTK"
            else:
               raise RuntimeError("Only linear, polynomial and tree kernels are supported")
   return params

def parse_examples(datafile, ignore_non_vectors = True):
   """A generator of examples/support vectors in a data file/model for
   tree-like data. 
   
   For each line in *datafile* containing a valid example (i.e. non containing
   model metadata) yield an :class:`Example` object.
   
   If *ignore_non_vectors* is |True|, then also yield non-example lines as
   unformatted strings.
   """
   tk_example_pattern = re.compile(r"^([+.0-9A-Za-z_-]+?)\s+\|BT\|\s+(.+?)\|[EB][TV]\|.*$")
   with open(datafile) as file:
      for line in file:
         match = tk_example_pattern.match(line)
         if match:
            yield Example(line)
         else:
            if not ignore_non_vectors:
               yield line
               
def parse_linear_examples(datafile):
   """Parse a linear data model *datafile*.
   
   For each non-metadata line, return a pair *(x, y)* where 
   *x* is the label of the example (as a string) and *y* is the
   vector of linear features (as a string as well).
   """
   preamble_finished = False
   with open(datafile) as file:
      for line in file:
         if preamble_finished:
            yield line.strip().split(" ", 1)
         if line.find("each following line is a SV") != -1:
            preamble_finished = True
            

def norm(strnode, ker_par):
   """
   Calculate the norm of the tree encoded by the string *strnode*
   according to the kernel parameters in the :class:`KernelParams`
   instance *ker_par*.
   """
   tk = _native.TK(
      strnode.encode("utf8"),
      strnode.encode("utf8"),
      ker_par.kernel,
      ker_par.decay_lambda,
      ker_par.decay_mu,
      False)
   return(sqrt(tk))

def count_fragments(strnode, ker_par):
   """
   Count the fragments in the tree encoded by the string *strnode*
   according to the kernel parameters in the :class:`KernelParams`
   instance *ker_par*.
   """
   tk = _native.TK(
      strnode,
      strnode,
      ker_par.kernel,
      1,
      1,
      False)
   return int(tk)
   
def TK(tree_a, tree_b, ker_par):
   """
   Calculate the tree kernel product between the trees encoded by the 
   strings *tree_a* and *tree_b* according to the kernel parameters in 
   the :class:`KernelParams` instance *ker_par*.
   """
   return _native.TK( 
      tree_a.encode("utf8"), 
      tree_b.encode("utf8"),
      ker_par.kernel,
      ker_par.decay_lambda,
      ker_par.decay_mu,
      ker_par.normalize)

def learn(data, model, params, stdoutfile = None, stderrfile = None):
   """
   Python interface to ``svm_learn``.
   
   *data* is the training file, *model* is the name of the output
   model and *params* is the string of command line options and
   flags for ``svm_learn``.
   
   If *stdoutfile* is not |None|, then redirect standard output 
   to that file. Otherwise, redirect it to ``model + ".stdout"``.
   
   If *stderrfile* is not |None|, then redirect standard error 
   to that file. Otherwise, redirect it to ``model + ".stderr"``.
   """
   if stdoutfile is None:
      stdoutfile = abspath(model) + ".stdout"
   if stderrfile is None:
      stderrfile = abspath(model) + ".stderr"
   if props.get_property("pytk_use_native"):
      sys.stdout.flush()
      sys.stderr.flush()
      result = _native.learn(data, model, params, stdoutfile, stderrfile)
      sys.stdout.flush()
      sys.stderr.flush()
   else:
      proc = Popen([path_to_svm_learn()] + params.split(" ") + [data, model], stdout = open(stdoutfile, "w"), stderr = open(stderrfile, "w"), close_fds = True)
      proc.wait()
      result = proc.returncode
   return result

def classify(data, model, predictions, stdoutfile = None, stderrfile = None):
   """
   Python interface to ``svm_classify``.
   
   *data* is the test file, *model* is the model to be used for
   classification and *predictions* is the output file of classifier
   decisions.
   
   If *stdoutfile* is not |None|, then redirect standard output 
   to that file. Otherwise, redirect it to ``predictions + ".stdout"``.
   
   If *stderrfile* is not |None|, then redirect standard error 
   to that file. Otherwise, redirect it to ``predictions + ".stderr"``.
   """
   if stdoutfile is None:
      stdoutfile = abspath(predictions) + ".stdout"
   if stderrfile is None:
      stderrfile = abspath(predictions) + ".stderr"
   if props.get_property("pytk_use_native"):
      sys.stdout.flush()
      sys.stderr.flush()
      result = _native.classify(data, model, predictions, stdoutfile, stderrfile)
      sys.stdout.flush()
      sys.stderr.flush()
      return result
   else:
      proc = Popen([path_to_svm_classify(), data, model, predictions], stdout = open(stdoutfile, "w"), stderr = open(stderrfile, "w"), close_fds = True)
      proc.wait()
      return proc.returncode

def get_tk_model_norm(model):
   """
   Reads the gradient norm of the separating hyperplane
   from the standard out file produced while learning the model *model*.
   """
   modelout = model + ".stdout"
   with open(modelout) as reader:
      for line in reader:
         if line.startswith("Norm of weight vector: |w|="):
            return round(float(line.split("=")[1]), 2)
   return None
         
def get_factories(kernel_name):
   """Returns the pair of classes to be used to handle,
   respectively, fragment generation and fragment indexing
   for the given kernel type.
   """
   frag_name = kernel_name + "_frag"
   idx_name = kernel_name + "_idx"
   exec("from flink.pytk.fragments.{0} import {0}".format(frag_name))
   exec("from flink.pytk.fragments.{0} import {0}".format(idx_name))
   return eval("({0},{1})".format(frag_name,idx_name))

def get_frag_module(kernel_name):
   frag_name = kernel_name + "_frag"
   exec("from flink.pytk.fragments import {0}".format(frag_name))
   return eval("{0}".format(frag_name))

def path_to_svm_classify():
   """
   Return the path to the executable for *svm_classify*.
   """
   return join(dirname(__file__), "native", "svm_classify")

def path_to_svm_learn():
   """
   Return the path to the executable for *svm_learn*.
   """
   return join(dirname(__file__), "native", "svm_learn")


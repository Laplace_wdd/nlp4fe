from flink.pytk import CONST_MAX_CHILDREN
from flink.pytk.defs.Fragment cimport Fragment
from flink.utils.tree cimport Node

PTK_MINFRAG_NUM_NODES = 1
PTK_MINFRAG_SEQ_LENGTH = 1

cpdef public double get_gradient_component(object kernel_params, double example_norm, int num_nodes, double seq_length):
   cdef double lambda_exp = seq_length
   cdef double mu_exp = num_nodes/2.0
   cdef double num = (kernel_params.decay_mu ** mu_exp) * (kernel_params.decay_lambda ** lambda_exp)
   cdef double result
   if not kernel_params.normalize:
      example_norm = 1.0
   result = num / example_norm
   #print "seq_length", seq_length, "num_nodes", num_nodes, \
   #      "example_norm", example_norm, "lambda_exp", lambda_exp, \
   #      "mu_exp", mu_exp, "num", num, "result", result
   return result

cdef class PTK_frag(Fragment):

   cdef public int init_base_frag(self, Node node):
      cdef Node c
      self.expanded_nodes = (node,)
      self.expanded_offsets = tuple()
      self.productions = tuple()
      self.toExpand = [(c, 0) for c in node.children]
      self.num_nodes = PTK_MINFRAG_NUM_NODES
      self.seq_length = PTK_MINFRAG_SEQ_LENGTH
      return 1
      
   cdef public double get_relevance(self):
      cdef double grad_component = get_gradient_component(
         self.kernel_params, self.example_norm, self.num_nodes, self.seq_length)
      return self.example_weight * grad_component
      #BAD BAD BAD just for test!
      #return grad_component

   cdef public Fragment _expand(self, tuple offsets):
      cdef PTK_frag frag
      cdef list expanded_nodes = [], toExpand = []
      cdef dict productions = {}
      cdef set expanded_offsets = set()
      cdef int offindex = 0, prod_offset, offset_count
      cdef Node n, x
      cdef list temp_offsets 
      frag = PTK_frag(self.example_weight, self.example_norm, self.kernel_params)
      frag.parent_frag = self
      frag.seq_length = self.seq_length
      frag.num_nodes = self.num_nodes
      frag.toExpand = []

      offset_count = 0
      for offset, (node, parent_offset) in enumerate(self.toExpand):
         if offset in offsets:
            if parent_offset not in productions:
               productions[parent_offset] = []
            # Honor the value of the MAX_CHILDREN constant
            if len(productions[parent_offset]) >= CONST_MAX_CHILDREN:
               return None
            productions[parent_offset].append(node)
            expanded_nodes.append(node)
            toExpand.extend([(x, offset_count) for x in node.children])
            offset_count += 1

      flattened_productions = []
      temp_offsets = []
      #print "TO_EXPAND", [(str(t),v) for t,v in self.toExpand]
      for offset,key in enumerate(sorted(productions)):
         #print "OFFSET", offset, "KEY", key
         #print "PRODUCTIONS", sorted(productions)
         flattened_productions.extend(productions[key])
         #update the value of the seq_length and num_nodes properties
         
         # VERSION_WORKING_IN_PRACTICE_WITH_CURRENT_IMPLEMENTATION 
         #frag.seq_length += (productions[key][-1].child_offset - productions[key][0].child_offset) + len(productions[key])/2.0
         
         # THEORETICALLY_CORRECT_VERSION 
         frag.seq_length += (productions[key][-1].child_offset - productions[key][0].child_offset) + len(productions[key])
         #frag.seq_length += (productions[key][-1].child_offset - productions[key][0].child_offset) + 1
         frag.num_nodes += len(productions[key])
         temp_offsets.append((key,len(productions[key])))

      frag.productions = tuple(flattened_productions)
      frag.toExpand = toExpand
      frag.expanded_nodes = tuple(expanded_nodes)
      frag.expanded_offsets = tuple(temp_offsets)
      #print "FRAG", str(frag)
      #print

      return frag


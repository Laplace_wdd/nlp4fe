from flink.pytk.defs.Fragment cimport Fragment
from flink.utils.tree cimport Node

cpdef public double get_gradient_component(object kernel_params, double example_norm, int num_nodes, double seq_length)

cdef class PTK_frag(Fragment):
   cdef public tuple productions
   cdef public int num_nodes
   cdef public double seq_length


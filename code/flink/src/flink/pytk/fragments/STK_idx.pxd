from flink.pytk.defs.FragIdx cimport FragIdx, IdxNode
from flink.pytk.defs.Fragment cimport Fragment
from flink.utils.tree cimport Node

cdef class STK_idx(FragIdx):
   cdef void _lookup(self, IdxNode cp, list n, int ap, dict res)

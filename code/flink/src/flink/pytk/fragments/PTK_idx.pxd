from flink.pytk.defs.FragIdx cimport FragIdx, IdxNode
from flink.pytk.defs.Fragment cimport Fragment
from flink.utils.tree cimport Node

cdef long unique_id(int x, int y)

cdef class PTK_idx(FragIdx):
   cdef void _lookup(self, IdxNode cp, list n, int sl, int nn, dict res)

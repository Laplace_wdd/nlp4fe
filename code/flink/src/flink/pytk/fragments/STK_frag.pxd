from flink.pytk.defs.Fragment cimport Fragment
from flink.utils.tree cimport Node

cpdef public double get_gradient_component(object kernel_params, double example_norm, int active_prod)

cdef class STK_frag(Fragment):
   cdef public tuple productions
   cdef public int active_productions


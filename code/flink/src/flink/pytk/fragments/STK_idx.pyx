from flink.pytk.defs.FragIdx cimport FragIdx, IdxNode
from flink.utils.tree cimport Node
from flink.pytk.defs.RelevanceData cimport RelevanceData
from flink.pytk.defs.Fragment cimport Fragment
from flink.pytk.fragments.STK_frag cimport STK_frag
import cython

cdef class STK_idx(FragIdx):

   def __reduce__(self):
      return FragIdx.__reduce__(self)

   cdef dict lookup(self, Node node, int recursive = 1):
      result = {}
      nodes = [node]
      if recursive:
         nodes = node.nodesIterator(True)
      for x in nodes:
         if (x.label,) in self.frags:
            self._lookup(self.frags[(x.label,)], [x], 0, result)
      return result

   cdef void _lookup(self, IdxNode current_pos, list nodes, int active_productions, dict result):
      cdef tuple expansion
      cdef list next_nodes
      cdef list next_labels
      cdef int offset
      for expansion in current_pos:
         if expansion[-1] >= len(nodes):
            continue
         next_nodes = []
         next_labels = []
         #print([x.label for x in nodes], expansion)
         for offset in expansion:
            next_nodes.extend(nodes[offset].children)
            next_labels.extend([x.label for x in nodes[offset].children])
            if offset != expansion[-1]:
               next_labels.append(None)
               next_nodes.append(None)
         key = tuple(next_labels)
         if key in current_pos[expansion]:
            active_prod_next = active_productions + len(expansion)
            if current_pos[expansion][key].data is not None:
               nid = current_pos[expansion][key].nid
               if nid not in result:
                  result[nid] = {"occurrencies": 0, 
                                "active_productions": active_prod_next}
               result[nid]["occurrencies"] += 1
            self._lookup(current_pos[expansion][key], next_nodes, active_prod_next, result)
   
if __name__ == "__main__":
   from flink.pytk.fragments.STK_frag import STK_frag
   from flink.utils.tree cimport parse
   index = STK_idx()
   tree = parse("(a(b(c)(d))(e(f)(g)))")
   for node in tree.nodesIterator():
      for frag in STK_frag.get_base_frag(node, 1):
         index.putFragment(frag)
         for fragment in frag.expansions({"lambda" : 0.4}, len(frag.toExpand)):
            index.putFragment(fragment)
         for fragment in frag.expansions({"lambda" : 0.4}, len(frag.toExpand)-1):
            index.putFragment(fragment)
   index.dump()
   
   tree2 = parse("(a(b)(e(f)(g)))")
   print("Looking up:", tree2)
   print(index.lookup(tree2))


from flink.pytk.defs.FragIdx cimport FragIdx, IdxNode
from flink.utils.tree cimport Node
from flink.pytk.defs.RelevanceData cimport RelevanceData
from flink.pytk.defs.Fragment cimport Fragment
from flink.pytk.fragments.PTK_frag cimport PTK_frag
from flink.pytk.fragments.PTK_frag import PTK_MINFRAG_SEQ_LENGTH, PTK_MINFRAG_NUM_NODES
import cython

from itertools import product, combinations
from flink.utils import mathutils
from math import sin, cos, atan

#def accumulate(x):
#   result = 0.0
#   if x.data is not None:
#      result += x.data.relevance ** 2.0
#   for k,v in x.items():
#      result += accumulate(v)
#   return result


cdef long unique_id(int x, int y):
      """Thanks to http://board.flashkit.com/board/showthread.php?t=805270"""
      # x = nid
      # y = seq_length
      return long((x**2+(y)**2)+( sin(atan(x/(y))) * cos(atan(x/(y)))))

cdef class PTK_idx(FragIdx):

   def __reduce__(self):
      return FragIdx.__reduce__(self)

   cdef dict lookup(self, Node node, int recursive = 1):
      cdef dict result = {}
      cdef list nodes = [node]
      if recursive:
         nodes = node.nodesIterator(True)
      for x in nodes:
         if (x.label,) in self.frags:
            self._lookup(self.frags[(x.label,)], [x], PTK_MINFRAG_SEQ_LENGTH, PTK_MINFRAG_NUM_NODES, result)
            # DEBUG BLOCK
            #print "{0} {1} ".format(str(x), accumulate(self.frags[(x.label,)]))
      return result
   
   cdef void _lookup(self, IdxNode current_pos, list nodes, int seq_length, int num_nodes, dict result):
      cdef list next_nodes, next_labels, allofthem
      cdef tuple expansion, temp_tuple, inner_tuple
      cdef int offset, a, start, end, nid, next_num_nodes, next_seq_length
      cdef long uid
      cdef Node node
      cdef int incompat
      if current_pos.data is not None:
         nid = current_pos.nid
         uid = unique_id(nid, seq_length)
         #print uid
         if uid not in result:
            result[uid] = {"occurrencies": 0, 
                          "seq_length": seq_length,
                          "num_nodes": num_nodes,
                          "real_nid": nid}
         result[uid]["occurrencies"] += 1
      for expansion in current_pos:
         incompat = 0
         for offset,num in expansion:
            if offset >= len(nodes) or num > len(nodes[offset].children):
               incompat = 1
               break
         if incompat:
            #print [str(x) for x in nodes], expansion, "INCOMPAT"
            continue
         allofthem = [combinations(nodes[x].children, y) for x,y in expansion]
         for temp_tuple in product(*allofthem):
            next_labels = []
            next_nodes = []
            next_num_nodes = num_nodes
            next_seq_length = seq_length
            for inner_tuple in temp_tuple:
               next_nodes.extend(inner_tuple)
               next_labels.extend([node.label for node in inner_tuple])
            key = tuple(next_labels)
            if key in current_pos[expansion]:
               #calculate the length of the sequence
               start = 0
               for a,end in expansion:
                  next_num_nodes += end
                  next_seq_length += (next_nodes[end-1].child_offset -
                        next_nodes[start].child_offset) + end  #end/2.0 # it was +1 before
                        #next_nodes[start].child_offset) + 1  #end/2.0 # it was +1 before
                        # THEORETICALLY_CORRECT_VERSION next_nodes[start].child_offset) + end # it was +1 before

                        # It would be something like +1 -1 + end
                        # where:
                        #  +1 comes directly from the definition of the subseq. length
                        #  -1 accounts for the fact that the contribution of the
                        #     expanded node to the length of the sequence is replaced
                        #     by the contribution of the sequence of children
                        #  +end accounts for the fact that each included node has a +1
                        #     contribution (always from the definition of the term in
                        #     the equation, since nodes with no children contribute +1
                        #     to the sequence length)
                  start += end
               self._lookup(current_pos[expansion][key], next_nodes, next_seq_length, next_num_nodes, result)
   

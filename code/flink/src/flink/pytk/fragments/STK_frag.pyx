from flink.pytk.defs.Fragment cimport Fragment
from flink.utils.tree cimport Node

cpdef public double get_gradient_component(object kernel_params, double example_norm, int active_productions):
   cdef double lambda_exp = active_productions/2.0
   cdef double num = kernel_params.decay_lambda ** lambda_exp
   if not kernel_params.normalize:
      example_norm = 1.0
   return num / example_norm

cdef class STK_frag(Fragment):

   cdef public int init_base_frag(self, Node node):
      cdef Node c
      if node.isLeaf():
         return 0
      self.expanded_nodes = (node,)
      self.expanded_offsets = (0,)
      self.productions = tuple(node.children)
      self.toExpand = [c for c in node.children if not c.isLeaf()]
      self.active_productions = 0
      return 1
      
   cdef public double get_relevance(self):
      cdef double grad_component = get_gradient_component(
         self.kernel_params, self.example_norm, self.active_productions)
      return self.example_weight * grad_component
      
   cdef public Fragment _expand(self, tuple offsets):
      cdef STK_frag frag
      cdef list expanded_offsets = [], expanded_nodes = [], productions = []
      cdef int offindex = 0, prod_offset
      cdef Node n, x
      frag = STK_frag(self.example_weight, self.example_norm, self.kernel_params)
      frag.parent_frag = self
      frag.toExpand = []
      for prod_offset,n in enumerate(self.productions):
         expanded_nodes.append(n)
         if n is not None:
            if not n.isLeaf():
               if offindex in offsets:
                  expanded_offsets.append(prod_offset)
                  frag.toExpand.extend([x for x in n.children if not x.isLeaf()])
                  productions.extend(n.children)
                  if len(productions) > 0 and offindex != offsets[-1]:
                     productions.append(None)
               offindex += 1
      frag.expanded_offsets = tuple(expanded_offsets)
      frag.expanded_nodes = tuple(expanded_nodes)
      frag.productions = tuple(productions)
      frag.active_productions = self.active_productions + len(offsets)
      return frag
   

from flink.pytk import Model
from flink.utils.logging import debug_msg
from os import times
from flink.utils import persistency
from random import choice

from flink.pytk.defs.Fragment cimport Fragment
from flink.pytk.defs.FragIdx cimport FragIdx
from flink.utils.tree cimport Node, parse

def greedy_miner(str model, str dictionary, double threshold, int min_frequency):
   cdef FragIdx this_iteration, that_iteration
   cdef Node treenode, node
   cdef Fragment frag, prevFrag
   cdef str example_label, example_data
   cdef float example_weight, example_norm
   cdef int value, iteration, stop_going_deeper, stop_going_wider
   cdef int idxnid, indexed_nid
   cdef set values, search_larger
   cdef dict next_depth_frags, prev_depth_frags, this_width_frags
   cdef object model_handler = Model(model)
   cdef object kernel_params = model_handler.get_kernel_params()
   if dictionary is None:
      dictionary = model + ".dict"
   frag_class,idx_class = model_handler.get_factories()
   next_depth_frags = {}
   this_iteration = idx_class()
   for example_label, example_data in model_handler.support_vectors():
      example_norm = model_handler.get_example_norm(example_data)
      node = parse(example_data)
      example_weight = float(example_label)
      for treenode in node.nodesIterator(True):
         frag = frag_class(example_weight, example_norm, kernel_params)
         if frag.init_base_frag(treenode) == 0:
            continue
         idxnid = this_iteration.putFragment(frag).nid
         if idxnid not in next_depth_frags:
            next_depth_frags[idxnid] = []
         next_depth_frags[idxnid].append(frag)
            
   (min_rel,max_rel) = this_iteration.get_min_max()
   
   req_rel = max_rel / threshold
   
   debug_msg("MAX relevance: {0} Thr: {1} REQ relevance: {2}".format(max_rel, threshold,  req_rel))
   
   result = idx_class()
   prev_depth_frags = {}
   nid_map = this_iteration.filter(min_frequency, req_rel, result)
   for old_nid,new_nid in nid_map.items():
      try:
         prev_depth_frags[new_nid] = next_depth_frags[old_nid]
      except:
         raise "Missing fragment!"
   #del next_depth_frags
   #del nid_map
   #del this_iteration
   next_depth_frags = None
   nid_map = None
   this_iteration = None
   
   iteration = 2
   stop_going_deeper = False
   while not stop_going_deeper:
      added = 0
      newfrags = 0
      debug_msg("Depth: {0} Num Frags: {1}".format(iteration, result.num_leaves))
      iter_before = sum(times()[0:2])
      next_depth_frags = {}
      stop_going_wider = False
      cardinality = 0
      search_larger = None
      while not stop_going_wider:
         debug_msg("Cardinality: {0}".format(cardinality))
         this_width_frags = {}
         that_iteration = idx_class()
         for indexed_nid in prev_depth_frags:
            for prevFrag in prev_depth_frags[indexed_nid]:
               if search_larger is not None and id(prevFrag) not in search_larger:
                  continue
               values = set()
               value = (prevFrag.maxPossibleExpansions() + 1) / 2
               if value == int(value):
                  lower = int(value)
                  upper = int(value)
               else:
                  lower = int(value)
                  upper = int(value) + 1
               if upper + cardinality <= prevFrag.maxPossibleExpansions():
                  values.add(upper + cardinality)
               if lower - cardinality >= 1:
                  values.add(lower - cardinality)
               for value in values:
                  for frag in prevFrag.expansions(value):
                     idxnode = that_iteration.putFragment(frag)
                     if idxnode.nid not in this_width_frags:
                        this_width_frags[idxnode.nid] = []
                     this_width_frags[idxnode.nid].append(frag)
         search_larger = set()
         stop_going_wider = True

         newfrags += that_iteration.size()
         size_before = result.size()
         nid_map = that_iteration.filter(min_frequency, req_rel, result)
         size_after = result.size() - size_before
         if size_after > size_before:
            added += size_after - size_before
            stop_going_wider = False
         for (old_nid, new_nid) in nid_map.items():
            try:
               next_depth_frags[new_nid] = this_width_frags[old_nid]
               search_larger.update([id(x) for x in this_width_frags[old_nid]])
            except:
               raise "Missing fragment!"
         #del this_width_frags
         #del that_iteration
         #del nid_map
         this_width_frags = None
         that_iteration = None
         nid_map = None
         cardinality += 1
      prev_depth_frags = next_depth_frags
      #del next_depth_frags
      next_depth_frags = None
      
      debug_msg("Added {0}/{1} fragments during this iteration".format(added, newfrags))
      debug_msg("Iteration time: {0}".format(sum(times()[0:2]) - iter_before))
      iteration += 1
      
      stop_going_deeper = (added == 0)
      
   persistency.save(result, dictionary)
   debug_msg("{0} fragments stored in dictionary file {1}".format(result.num_leaves, dictionary))
   #del result
   result = None
   return dictionary

def full_miner(str model, str dictionary = None, int max_expansions = 0):
   if dictionary is None:
      dictionary = model + ".dict"
   model_handler = Model(model)
   kernel_params = model_handler.get_kernel_params()
   frag_class,idx_class = model_handler.get_factories()
   cdef FragIdx result = idx_class()
   cdef Node node, treenode
   cdef Fragment base, to_expand
   cdef str example_data, example_label
   cdef float example_weight, example_norm
   for (example_label, example_data) in model_handler.support_vectors():
      example_norm = model_handler.get_example_norm(example_data)
      example_weight = float(example_label)
      node = parse(example_data)
      for treenode in node.nodesIterator(True):
         next = []
         base = frag_class(example_weight, example_norm, kernel_params)
         if base.init_base_frag(treenode) == 0:
            continue
         if base is not None:
            next.append(base)
         while len(next) > 0:
            to_expand = next.pop()
            result.putFragment(to_expand)
            if max_expansions == 0 or to_expand.get_times_expanded() < max_expansions: 
               for num_exp in range(1, to_expand.maxPossibleExpansions()+1):
                  next.extend(to_expand.expansions(num_exp))
   persistency.save(result, dictionary)
   #result.dump()
   return dictionary

def random_miner(model, dictionary = None, num_frags = 100):
   if dictionary is None:
      dictionary = model + ".dict"
   model_handler = Model(model)
   kernel_params = model_handler.get_kernel_params()
   frag_class,idx_class = model_handler.get_factories()
   result = idx_class()
   examples = []
   for (example_label, example_data) in model_handler.support_vectors():
      norm = model_handler.get_example_norm(example_data)
      weight = float(example_label)
      node = parse(example_data)
      examples.append( {"tree": node, "weight": weight, "norm": norm})
   while num_frags > 0: 
      selected = choice(examples)
      include_leaves = True if kernel_params.kernel_name == "PTK" else False
      nodes = [x for x in selected["tree"].nodesIterator(include_leaves)]
      if len(nodes) == 0:
         continue
      node = choice(nodes)
      current_frag = frag_class(selected["weight"], selected["norm"], kernel_params)
      if current_frag.init_base_frag(node) == 0:
         continue
      while True:
         num_expansions = [x for x in range(0, current_frag.maxPossibleExpansions()+1)]
         if len(num_expansions) == 1:
            # fragment cannot be further expanded
            result.putFragment(current_frag)
            num_frags -= 1
            break
         else:
            num_exp = choice(num_expansions)
            if num_exp == 0:
               result.putFragment(current_frag)
               num_frags -= 1
               break
            else:
               current_frag = choice(current_frag.expansions(num_exp))
   persistency.save(result, dictionary)
   return dictionary
            

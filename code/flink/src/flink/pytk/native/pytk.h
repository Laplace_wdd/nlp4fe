#include <string.h>
#include <math.h>
#include <svm_common.h>

int getdef_MAX_CHILDREN(void);
void make_doc(char* tree, DOC* doc, KERNEL_PARM* param);
double TK(char* tree_a, char* tree_b, long type, double lambda, double mu, short normalize);
int learn(char* ex, char* model, char* args, char* o, char* e);
int classify(char* ex, char* model, char* pred, char* o, char* e);


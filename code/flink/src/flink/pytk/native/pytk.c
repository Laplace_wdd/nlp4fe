#include "pytk.h"

extern int svm_classify_main(int, char**);
extern int svm_learn_main(int, char**);
extern double LAMBDA, LAMBDA2, MU, SIGMA, REMOVE_LEAVES;
extern short TKGENERALITY;
extern double tree_kernel_not_norm(KERNEL_PARM*, DOC*, DOC*, int, int);
extern void freeForest(DOC*);


int getdef_MAX_CHILDREN(void)
{
   return MAX_CHILDREN;
}

void make_doc(char* tree, DOC* doc, KERNEL_PARM* param)
{
   char line[10240] = "1 |BT| ";
   strcat(line, tree);
   strcat(line, " |ET|\n");
   double doc_label[1];
   long int x=0, y=0;
   if(!parse_document(line, doc, doc_label, &x, y, param)) {
      printf("\nParsing error for line: %s\n",line);
      exit(1);
   }
}

double TK(char* tree_a, char* tree_b, long type, double lambda, double mu, short normalize)
{
   KERNEL_PARM param;
   param.lambda = lambda;
   param.mu = mu;
   param.first_kernel = type;
   DOC a, b;
   make_doc(tree_a, &a, &param);
   make_doc(tree_b, &b, &param);

   SIGMA = 1;
   LAMBDA = param.lambda;
   MU = param.mu;
   LAMBDA2 = LAMBDA*LAMBDA;
   REMOVE_LEAVES=0;
   TKGENERALITY = param.first_kernel;

   //printf("%s %s\n", tree_a, tree_b);

   double tk = tree_kernel_not_norm(&param, &a, &b, 0, 0);

   //printf("%lf\n", tk);
   if (normalize) { 
      tk /=  sqrt(tree_kernel_not_norm(&param, &a, &a, 0, 0) *
               tree_kernel_not_norm(&param, &b, &b, 0, 0));
   }

   freeForest(&a);
   freeForest(&b);

   return tk;
}

int learn(char* ex, char* model, char* args, char* outfile, char* errfile)
{
   fflush(stdout);
   fflush(stderr);
   FILE* 
   tmpout = stdout;
   FILE* tmperr = stderr;
   stdout = fopen(outfile, "w");
   stderr = fopen(errfile, "w");
   char dupargs[1000];
   strcpy(dupargs, args);
   char* arr[100];
   arr[0] = "X";
   int i = 1;
   arr[i] = strtok(dupargs, " ");
   while (arr[i] != NULL) {
      i++;
      arr[i] = strtok(NULL, " ");
   }
   arr[i++] = ex;
   arr[i++] = model;
   int numargs = i;
   int result = svm_learn_main(numargs, arr);
   fflush(stdout);
   fflush(stderr);
   fclose(stdout);
   fclose(stderr);
   stdout = tmpout;
   stderr = tmperr;
   return result;
}

int classify(char* ex, char* model, char* pred, char* outfile, char* errfile)
{
   fflush(stdout);
   fflush(stderr);
   FILE* tmpout = stdout;
   FILE* tmperr = stderr;
   stdout = fopen(outfile, "w");
   stderr = fopen(errfile, "w");
   char* arr[4] = {"X", ex, model, pred};
   int result = svm_classify_main(4, arr);
   fflush(stdout);
   fflush(stderr);
   stdout = tmpout;
   stderr = tmperr;
   return result;
}


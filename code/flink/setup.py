#!/usr/bin/env python3

# To build cython extensions from scratch, define the environment variable USE_CYTHON
# when invoking setup, e.g.:
#
#   USE_CYTHON=1 python3 setup.py build_ext
#

import sys

from os import getenv
from distutils.dist import Distribution
from distutils.core import setup
from distutils.dir_util import copy_tree, mkpath
from distutils.file_util import copy_file
from distutils.extension import Extension
from distutils.archive_util import make_archive
from distutils.command.build import build as DistutilsBuild
from distutils.command.sdist import sdist as DistutilsSdist
from subprocess import call
import defs

cython_ext_pairs = [
       ("flink.utils.tree", ["src/flink/utils/tree.pyx"]),
       ("flink.pytk.defs.Fragment", ["src/flink/pytk/defs/Fragment.pyx"]),
       ("flink.pytk.fragments.STK_frag", ["src/flink/pytk/fragments/STK_frag.pyx"]),
       ("flink.pytk.fragments.PTK_frag", ["src/flink/pytk/fragments/PTK_frag.pyx"]),
       ("flink.pytk.defs.RelevanceData", ["src/flink/pytk/defs/RelevanceData.pyx"]),
       ("flink.pytk.defs.FragIdx", ["src/flink/pytk/defs/FragIdx.pyx"]),
       ("flink.pytk.fragments.STK_idx", ["src/flink/pytk/fragments/STK_idx.pyx"]),
       ("flink.pytk.fragments.PTK_idx", ["src/flink/pytk/fragments/PTK_idx.pyx"]),
       ("flink.pytk.miners", ["src/flink/pytk/miners.pyx"]),
       ("flink.private.activities", ["src/flink/private/activities.pyx"]),
       ("flink.activities", ["src/flink/activities.pyx"]),
       ]

USE_CYTHON = getenv("USE_CYTHON")

if USE_CYTHON:
   from Cython.Distutils import build_ext
   cython_exts = [
         Extension(name = x[0], sources = x[1], 
            extra_compile_args = ["-O3", "-Wall"]) 
         for x in cython_ext_pairs]
else:
   from distutils.command.build_ext import build_ext
   cython_exts = [Extension(name = x[0], 
         sources = [y.replace(".pyx", ".c") for y in x[1]]) 
         for x in cython_ext_pairs]


# Hack to get output build dir
# (source: http://stackoverflow.com/questions/4687608/using-sphinx-with-a-distutils-built-c-extension/4704120#4704120)
b = DistutilsBuild(Distribution())
b.initialize_options()
b.finalize_options()
BUILD_DIR = b.build_platlib
   
def run_command(name, args, cwd = "."):
   logfile = ".{0}.log".format(name)
   with open(logfile, "w") as logf:
      print("Building {0}...".format(name), end=" ")
      sys.stdout.flush()
      exit_code = call(args, cwd=cwd, stdout=logf, stderr=logf)
   if exit_code != 0:
      print("\nFailed building {0}.".format(name))
      print("A complete error log is available in", logfile)
      sys.exit(1)
   else:
      print("done.")

class MyBuild(DistutilsBuild):
   def run(self):
      native_lib = "pytk.so"
      run_command("svm_light-native", ["make"], "src/flink/pytk/native")
      DistutilsBuild.run(self)
      #copy_tree("src/flink/pytk/native", BUILD_DIR + "/flink/pytk/native")
      mkpath(BUILD_DIR + "/flink/pytk/native")
      copy_file("src/flink/pytk/native/" + native_lib, BUILD_DIR + "/flink/pytk/native")
      copy_file("src/flink/pytk/native/svm_learn", BUILD_DIR + "/flink/pytk/native")
      copy_file("src/flink/pytk/native/svm_classify", BUILD_DIR + "/flink/pytk/native")

class MySdist(DistutilsSdist):
   def run(self):
      global data_files
      run_command("docs", ["make", "html"], "sphinxdoc")
      make_archive(
         "html_docs", "gztar", "sphinxdoc/_build", "html")
      DistutilsSdist.run(self)

setup(
    name = defs.FLINK_DEFS_PROJECT.lower(),
    version = defs.FLINK_DEFS_VERSION,
    author = defs.FLINK_DEFS_AUTHOR,
    author_email = defs.FLINK_DEFS_EMAIL,
    url = defs.FLINK_DEFS_URL,
    package_dir = {"": "src"},
    license = "Free for non-commercial use (see LICENSE file)",
    description = "FLinK is a Framework for the Linearization of Kernel functions",
    long_description = \
                  "Flink allows for fast linear classifiers explicitly " \
                  "encoding rich features mined in high-dimensional " \
                  "kernel spaces",
    packages = ["flink", "flink.linearize", "flink.private", "flink.utils", 
                "flink.pytk", "flink.pytk.defs", "flink.pytk.fragments"],
    package_data = {"flink.pytk": ["src/flink/pytk/native/*.so", 
       "src/flink/pytk/native/SVM-Light-TK-1.5/svm_learn",
       "src/flink/pytk/native/SVM-Light-TK-1.5/svm_classify"]},
    cmdclass = {'build_ext': build_ext,
                'sdist': MySdist,
                'build': MyBuild },
    ext_modules = cython_exts
)

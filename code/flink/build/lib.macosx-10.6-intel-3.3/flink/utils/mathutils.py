"""
*******************************************************
:mod:`flink.utils.mathutils` --- mathematical functions
*******************************************************
"""

from math import sqrt

class dense_vector(dict):
   """
   Dict-like dense representation for vectors.
   
   Dictionary entries are in the form ``int: float`` and are
   meant to represent components and their values.
   A missing key is regarded as a component whose value is 0.
   
   *initial* is a dictionary that can be used to initialize the value
   of some components of the vector.
   """
   def __init__(self, initial = {}):
      dict.__init__(self, initial)
   
   @staticmethod
   def parse(vectstring):
      """
      Return a new dense vector by parsing the string *vecstring*, which is
      supposed to be in the form::
      
         component_id:value [component_id:value ...]
      """
      result = {}
      for x in vectstring.split():
         data = x.split(":")
         result[int(data[0])] = float(data[1])
      return dense_vector(result)
         
   def multiply(self, val):
      """
      Multiply all the components of this vector by the value *val*.
      """
      for k,v in self.items():
         self[k] = v * val 

   def sum(self, *dense_vectors):
      """
      Sum to this vector all the vectors in *dense_vectors*.
      """
      for vector in dense_vectors:
         for component, value in vector.items():
            if component not in self:
               self[component] = 0
            self[component] += value
            
   def norm(self):
      """
      Calculate the norm of this dense vector.
      """
      result = 0
      for v in self.values():
         result += v ** 2
      return sqrt(result)


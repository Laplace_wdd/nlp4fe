"""
***************************************************************
:mod:`flink.utils.fileutils` --- filesystem related functions
***************************************************************
"""

import shutil, sys
import os
from os.path import exists, dirname, basename, join
from tempfile import mkdtemp

def makedirs(outdir):
   """
   Create the directory *outdir* and all the necessary
   intermediate directories.
   """
   try:
      os.makedirs(outdir)
   except:
      pass
   
def copy_file(src, dest):
   """
   Copy *src* to *dest* after making sure that the destination
   directory exists.
   """
   makedirs(dirname(dest))
   shutil.copy(src, dest)
   
def move_file(src, dest):
   """
   Move *src* to *dest* after making sure that the destination
   directory exists.
   """
   makedirs(dirname(dest))
   shutil.move(src, dest)
   
def open_for_writing(filename, mode = "w"):
   """
   Return a file descriptor for *filename* in mode *mode* after
   making sure that the destination directory has been created.
   """
   makedirs(dirname(filename))
   return open(filename, mode)
   
def rmdir(dir):
   """
   Remove directory *dir*.
   """
   shutil.rmtree(dir, True)
   
def count_lines(inputfile):
   """
   Return the number of lines in *inputfile*.
   """
   file = open(inputfile)
   data = file.read()
   file.close()
   numLines = data.count("\n")
   if not data.endswith("\n"):
      numLines += 1
   return numLines 

def _copy_lines(sourcefile, numlines, destfile = None):
   count = 0
   while count < numlines:
      if destfile != None:
         destfile.write(sourcefile.readline())
      else:
         sourcefile.readline()
      count += 1
      
def _get_split_names(infile, num_splits, outdir):
   if num_splits == 1:
      return[join(outdir, basename(infile))]
   result = []
   for x in range(0, num_splits):
      base = join(outdir, basename(infile))
      result.append("{0}.split_{1:03d}_{2:03d}".format(base, x + 1, num_splits))
   return result

def split_file(infile, num_splits, outdir, balanced = False):
   """
   Partition *infile* into *num_splits* partitions.
   
   By default, the file is split sequentially, i.e. if *num_splits*
   equals 2, then the first half of the file is saved to a split and
   the second half to another split. If *balanced* is |True|, instead,
   the file is splitted so as to mantain the positive/negative example
   ratio in each split. 
   
   The partitions are created in *outdir*. Each partition has the same
   basename as *infile*, to which the extension ``.split_XXX_YYY`` is
   added. ``XXX`` is the zero padded identifier of the split, in the
   range [1-*num_splits*]. ``YYY`` is the zero padded *num_splits*.
   
   Return the sequence of split file paths.
   """
   split_func = _split_sequential
   if balanced:
      split_func = _split_balanced
   split_names = _get_split_names(infile, num_splits, outdir)
   if len(split_names) == 1:
      if split_names[0] != infile:
         copy_file(infile, split_names[0])
      return split_names
   split_func(infile, split_names)
   return split_names

def make_folds(infile, num_folds, outdir):
   """
   Create a cross-fold validation set from the datafile *infile*.
   
   The file is split into *num_folds* splits. For each split,
   assumed as test set, a corresponding training set is generated
   by concatenating all the other *num_folds*-1 folds.
   
   Return a dictionary of ``training: test`` files for each fold.
   """
   result = {}
   splits = split_file(infile, num_folds, outdir, balanced = True)
   for testfile in splits:
      trainfiles = [f for f in splits if f is not testfile]
      outfile = testfile + ".training"
      join_files(trainfiles, outfile)
      result[outfile] = testfile
   return result
         
def _split_sequential(inputfile, splits):
   numsplits = len(splits)
   # We have to generate at least one split, calculate number of lines
   lines = count_lines(inputfile)
   split_size = int(lines/numsplits)
   split_lines = []
   for split_id in range(1,numsplits + 1):
      if split_id == numsplits:
         split_lines.append(lines - (split_size*(numsplits-1)) )
      else:
         split_lines.append(split_size)
   # Actually do the splitting thing
   infile = open(inputfile)
   for (offset, name) in enumerate(splits):
      tocopy = split_lines[offset]
      outfile = open_for_writing(name, "w")
      _copy_lines(infile, tocopy, outfile)
      outfile.close()
   infile.close()
   
def _split_balanced(inputfile, splits):
   numsplits = len(splits)
   positive = []
   negative = []
   with open(inputfile) as infile:
      for line in infile:
         if line.strip().startswith("-"):
            negative.append(line)
         else:
            positive.append(line)
            
   outfiles = []
   for outname in splits:
      outfiles.append(open_for_writing(outname, "w"))
   for offset,x in enumerate(negative + positive):
      outfiles[offset%numsplits].write(x)
   for outfile in outfiles:
      outfile.close()
   return

def cat(filename, stream = sys.stdout):
   """
   Copy to the file descriptor *stream* (that must be open
   for writing) the contents of *filename*.
   """
   with open(filename) as file:
      for line in file:
         stream.write(line)

def join_files(splitfiles, outfile):
   """
   Concatenate all the files in *splitfiles* into the
   output file *outfile*.
   """
   if len(splitfiles) == 1:
      if splitfiles[0] != outfile:
         if exists(outfile):
            os.remove(outfile)
         shutil.copy(splitfiles[0], outfile)
      return outfile
   with open(outfile, "w") as out:
      for input in sorted(splitfiles):
         with open(input) as infile:
            for line in infile:
               out.write(line) 
   return outfile
               
def merge_binary_class_files(infiles, outfile, predictions_mode = False, discard = ".dat"):
   outdescr = open(outfile, "w")
   indescr = [open(x) for x in infiles]
   classes = [basename(x).replace(discard, "") for x in infiles]
   numlines = 0
   while True:
      best_offset = None
      best_score = -1000
      for offset,desc in enumerate(indescr):
         line = desc.readline().strip()
         if len(line) == 0:
            for x in indescr:
               x.close()
            outdescr.close()
            return numlines
         linedata = line.split(None, 1)
         label = float(linedata[0])
         if predictions_mode:
            if label > best_score:
               best_score = label
               best_offset = offset
         else:
            if label > 0:
               print("{0} {1}".format(classes[offset], linedata[1]), file = outdescr)
               numlines += 1
               continue
      if predictions_mode:
         print("{0}".format(classes[best_offset]), file = outdescr)
         numlines += 1

def scan_class_file_labels(datfile):
   labs = set()
   with open(datfile) as f:
      for line in f:
         label = line.split(None, 1)[0]
         labs.add(label)
   return labs
      
def generate_binary_class_files(infile, label_outfilename_map = {}):
   ofs = {}
   for x,y in label_outfilename_map.items():
      ofs[x] = open(y, "w")
   numlines = 0
   with open(infile) as inf:
      for line in inf:
         numlines += 1
         (lab, data) = line.strip().split(None, 1)
         for olab, ofile in ofs.items():
            outlab = "-1"
            if olab == lab:
               outlab = "+1"
            print("{0} {1}".format(outlab, data), file = ofile)
   for x in ofs.values():
      x.close()
   return numlines
   
   
"""
****************************************************************
:mod:`flink.utils.props` --- handle configuration properties
****************************************************************

API to access/modify the value of configiration properties
defined in the configuration file :mod:`flink.config`.
"""

import sys
_CONF_MODULE = "flink.config"
exec("import {0}".format(_CONF_MODULE))

def get_defined_properties():
   """Return a dictionary of the configuration properties defined
   in :mod:`flink.config`.
   """
   result = {}
   def_props = sys.modules[_CONF_MODULE].__dict__
   for x,v in def_props.items():
      if not x.startswith("_"):
         if type(v) in [int, float, str, bool]:
            result[x] = v
   return result

def set_property(name, value):
   """Set the value of a configuration property.
   
   The property must be already defined in the global configuration
   module.
   
   In case of success, returns a tuple *(old,new)* with the old and
   the new value of the property. 
   
   Raises :class:`NoSuchProperty` if the required property is not set,
   :class:`IncompatiblePropertyValue` if the value provided does not
   match the type of the property to be overwritten.
   """
   def_props = sys.modules[_CONF_MODULE].__dict__
   if name not in def_props:
      raise NoSuchProperty(name)
   try:
      cast_value = type(def_props[name])(value)
   except:
      raise IncompatiblePropertyValue(def_props[name], value)
   old = def_props[name]
   def_props[name] = cast_value
   return (old, cast_value)
   
def get_property(name):
   """
   Return the value of the configuration property *name*.
   
   Raise :class:`NoSuchProperty` if a configuration property of the
   given name does not exist.
   """
   props = get_defined_properties()
   if name not in props:
      raise NoSuchProperty(name)
   return props[name]

class NoSuchProperty(RuntimeError):
   """
   Raised when the undefined property *name* is accessed.
   """
   def __init__(self, name):
      RuntimeError.__init__(self, "No such config property: {0}".format(name))
      
class IncompatiblePropertyValue(RuntimeError):
   """
   Raised when trying to update the value *prop* of some property
   with the incompatible value *value*.
   """
   def __init__(self, prop, value):
      prop_type = type(prop).__name__
      value_type = type(value).__name__
      self.expected_type = prop_type
      self.received_type = value_type
      RuntimeError.__init__(self,
         "Impossible to cast value {0} from {1} to {2}".format(
            value, value_type, prop_type))


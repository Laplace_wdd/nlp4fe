"""
*******************************************************************
:mod:`flink.utils.pool` --- multiprocessing in |project| activities
*******************************************************************

The module provides multiprocessing facilities for flink modules.
"""

from os import times
from multiprocessing import Pool

class workers_pool:
   """
   A simple wrapper around :class:`multiprocessing.Pool`.
   
   *ncpu* is the maximum number of workers that can run at the same time.
   
   The pool is automatically bypassed if *ncpu* is 1.
   """
   def __init__(self, ncpu):
      if ncpu > 2:
         self._pool = Pool(processes = ncpu)
      else:
         self._pool = None
      self._results = []
      self._times = []
   
   def submit_job(self, func, kwargs):
      """
      Submit a new job to the pool.
      
      *func* is the function that the worker will run, and *kwargs* is
      dictonary of keyword arguments for *func*.
      """
      if self._pool:
         self._results.append(self._pool.apply_async(_slave_run, (func,kwargs)))
      else:
         self._results.append(_slave_run(func, kwargs))
      
   def get_results(self):
      """
      Return the list of results produced by the worker processes,
      in the order they were submitted to the pool.
      """
      if self._pool:
         self._pool.close()
         self._pool.join()
         result = [x.get()["result"] for x in self._results]
         return result
      else:
         return [x["result"] for x in self._results]
      
   def get_elapsed_time(self):
      """
      Return CPU time (user + system) required to run all the
      jobs in the pool
      """
      if self._pool:
         return sum([x.get()["elapsed_time"] for x in self._results])
      else:
         return sum([x["elapsed_time"] for x in self._results])
   
def _slave_run(func, args):
   begin = sum(times()[0:2])
   #debug_msg("Running '{0}' with args: {1}".format(func.__name__, args))
   result = func(**args)
   elapsed = sum(times()[0:2]) - begin
   return {"elapsed_time": elapsed, "result": result}


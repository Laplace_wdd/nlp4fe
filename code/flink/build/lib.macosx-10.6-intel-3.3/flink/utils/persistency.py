"""
***********************************************************************
:mod:`flink.utils.persistency` --- a facility for object persistency
***********************************************************************

A simple wrapper around :mod:`pickle` for object persistency.
"""

import pickle
from flink.utils import fileutils

def save(object, filename):
   """
   Save object *object* to the path identified by *filename*.
   Create the necessary directory hierarchy in the process.
   """
   out = fileutils.open_for_writing(filename, "wb")
   pickle.dump(object, out)
   out.close()
   
def load(filename):
   """
   Unpickle the object saved at *filename* and return it.
   """
   out = open(filename, "rb")
   result = pickle.load(out)
   out.close()
   return result
   
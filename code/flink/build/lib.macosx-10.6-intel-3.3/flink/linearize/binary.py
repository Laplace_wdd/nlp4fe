#!/usr/bin/env/python3

"""
Binary STK and PTK model linearization: :mod:`flink.linearize.binary`
---------------------------------------------------------------------

This module implements a greedy-miner based linearization system for
PTK and STK models.

It assumes that the models have already been learned using 
:mod:`flink.linearize.ksl` and that they are located in a directory
``<ksl_dir>``.

Depending on how the models were learned (single model vs. split model
configuration), this module implements either the *OPT* or the *Split*
linearization models described in :ref:`linearization-architectures`.

The *LIN* model, which like *OPT* requires non-split kernel space
learning, is provided as a special case of the *OPT* model via a
command line flag, as explained in :ref:`lin-model-evaluation`

Available command line options can be listed by invoking the module
with the ``-h`` flag::

   python3 -m flink.linearize.binary -h
   
The simplest command line for this module looks like::

   python3 -m flink.linearize.binary -k <ksl_dir> -o <outdir>
              <training> <test>
              
where:

   - ``<ksl_dir>`` is the output directory of :mod:`flink.linearize.ksl`
   - ``<outdir>`` is the selected output directory for the linearizer
   - ``<training>`` is the training data file
   - ``<test>`` is the test file to evaluate the model
   
.. note::

   ``<training>`` must be the same file used for kernel space learning!

In this case, the mining algorithm will be invoked with all the default
parameters, and learning in the linear space will be carried out with
the default cost factor of 1.

This behavior can be customized by changing the value of the following
command line options:

   - ``-t THRESHOLD`` and ``-f MIN_FREQUENCY``: the values of the threshold 
     factor and minimum fragment frequency parameters of the greedy miner
     algorithm :func:`flink.pytk.miners.greedy_miner`.
     
     If ``THRESHOLD`` is set to 0, then the module automatically carries
     out a cross-fold evaluation to estimate an appropriate value for
     the parameter. The optimization is carried out via
     :func:`flink.optimize.optimize_threshold` using the training set
     as benchmark.
     
     The number of folds to be used can be controlled via the option ``-a``
     (it defaults to 5).
     
   - ``-j COST_FACTOR``: cost factor for linear space learning. If set
     to 0, then the module automatically carries out cost factor optimization
     via :func:`flink.optimize.optimize_cost_factor`, using thre training
     set as a benchmark.
   
     The number of folds to be used can be controlled via the option ``-b``
     (it defaults to 5).  
     
The module will produce a lot of output in the selected directory
``<outdir>``.

Most notably, assuming that ``<bn_training>`` and ``<bn_test>`` are the
base names (i.e. file names without directory information) of ``<training>``
and ``<test>``, respectively:

   - the file ``<outdir>/dictionary.dict`` is the dictionary of the most
     relevant fragments mined from the models in ``<ksl_dir>`` for the
     given parameters of the mining algorithm;
     
   - the files ``<outdir>/<bn_training>.lin`` and ``<outdir>/<bn_test>.lin``
     are the linearized training and test set, respectively;
     
   - the file ``<outdir>/<bn_training>.lin.model-<params>`` is the model
     learned in the linear space from the linearized training set;
     
   - the file ``<outdir>/<bn_training>.lin.model-<params>.best_frags``
     contains a ranking of the most relevant fragments according to their
     weight as estimated by the learner in the linear space; 
     
   - the file ``<outdir>/<bn_test>.lin.pred`` contains the classifiers'
     predictions for the linearized test set;
     
   - the file ``<outdir>/<bn_test>.lin.pred.eval`` contains the evaluation
     of the predictions in ``<outdir>/<bn_test>.lin.pred``.
     
The module will also output to the terminal the results of the evaluation,
plus several other bits of information including the time necessary to
perform each sub-task.

.. _lin-model-evaluation:

*LIN* model evaluation
......................
     
Assuming that tree kernel learning was not carried out in a split fashion,
the module can also be used to evaluate the *LIN* model on the same
data set. 

To this end, it is possible to add the ``-L`` flag to the
command line of the module. In this case, it will also print to standard out
the evaluation of the LIN model on the same benchmark, as well as the
gradient norm of the linearized model.

The following files will also be present in the output
directory:

   - ``<outdir>/<bn_training>.lin.model-<params>.tk2lin``:
     the linearized model file;
     
   - ``<outdir>/<bn_test>.lin.pred_tk2lin``: test predictions according
     to the linearized model, and
     
   - ``<outdir>/<bn_test>.lin.pred_tk2lin.eval``: evaluation of the
     predictions obtained with the linearized model.
     

Tree kernel evaluation
......................

For convenience, :mod:`flink.linearize.binary` can also evaluate the
accuracy of the TK models learned during KSL. As in the previous case,
this feature is only available if kernel space learning was carried out
in a non-split fashion.

This can be accomplished by supplying the ``-T`` command line flag.

If the flag is present, the program will also output the results of 
classification using the TK model, as well as the gradient norm of the
TK model and the number of fragments in the model.

.. note::

   The number of fragments in the model is a very loose lower bound
   of the real number of fragments it encodes, as it is calculated
   as the number of fragments of the largest tree in the model. The
   real number of fragments in the model should generally be in
   the same order of magnitude.
   
The output directory will also contain the file ``<outdir>/<bn_test>.pred``
with the predictions generated according to the TK model.
"""

from flink.utils.cli import cliparse, group, int_option, str_option,\
   config_properties_option, flag, float_option
from flink.utils.logging import result_msg, info_msg
from flink.utils import persistency, fileutils
from flink.activities import learn, mine, linearize_file, classify, \
   evaluate, sort_features, calculate_linear_model_norm, \
   get_tk_model_norm, tk2lin, count_frags_in_model, find_models
from flink.optimize import optimize_threshold, optimize_cost_factor
from multiprocessing import cpu_count

def main(cmdline_args):
      
   #============================================================================
   # Parse command line arguments
   #============================================================================
      
   options,args = cliparse(
      
      group("Multiprocessing options"),
      
      int_option("n", "ncpu", help = "use up to N processes", default = cpu_count()), 
      
      group("Algorithm parameters"),
      
      str_option("k", "ksl_dir", help = "directory with TK models. "
         "Note that, to be recognized as such, a model filename must "
         "match the regular expression '.+?\.model[^.]$'"),
      float_option("t", "threshold", help = "miner threshold. If set to 0, perform "
         "cross-fold evaluation to estimate an appropriate value", default = 5.0),
      int_option("a", "num_folds_thropt", help = "Number of folds to be used "
         "for threshold cross-fold evaluation", default = 5),
      int_option("f", "min_frequency", 
         help = "miner min fragment frequency", default = 5),
      int_option("j", "cost_factor", help = "cost factor for linear "
         "learning. If set to 0, perform cross-fold evaluation to estimate "
         "an appropriate value", default = 1),
      int_option("b", "num_folds_cfopt", help = "Number of folds to be used "
         "for linear learning cost-factor cross-fold evaluation", default = 4),
      
      group("Output options"),
      
      str_option("o", "outdir", help = "output directory"),
      flag("c", "clean_outdir", help = "delete output directory befor running"),
      flag("T", "tk_evaluate", help = "also evaluate TK classifier "
           "(it actually works only for non-split KSL)"),
      flag("L", "lin_evaluate", help = "also evaluate LIN model "
           "(it actually works only for non-split KSL)"),
      
      group("General options"),
      
      config_properties_option(),
      
      cli_num_args = 2,
      args = cmdline_args,
      usage = "%prog [options] <training data file> <test data file>")
      
   train = args[0]
   test = args[1]
   
   opt_times = {}
   
   if options.clean_outdir:
      info_msg("Cleaning output directory {0}".format(options.outdir))
      fileutils.rmdir(options.outdir)
   
   tk_models, kernel_type = find_models(options.ksl_dir)
   
   if len(tk_models) == 0 or kernel_type == None:
      raise RuntimeError("No models found in KSL dir {0}".format(
         options.ksl_dir))
      
   if kernel_type not in ["STK", "PTK"]:
      raise RuntimeError("No tree kernel models in KSL dir {0}".format(
         options.ksl_dir))
   
   if len(tk_models) == 1:
      tk_models = tk_models[0]
   
   #============================================================================
   # Optimize threshold, hence carrying out KSM and LSG-train
   #============================================================================
   
   if options.threshold == 0:

      info_msg("Optimizing threshold parameter ({0}-fold)".format(options.num_folds_thropt))
      
      thropt_results, opt_times["THR_OPT"] = \
         optimize_threshold(options.outdir, tk_models, train, 
            num_folds = options.num_folds_thropt, 
            ncpu = options.ncpu, get_etime = True)
         
      dictionary, lin_tr, lin_tr_mapping, options.threshold = \
        thropt_results["dictionary"], \
        thropt_results["linearized_benchmark"], \
        thropt_results["nid_mapping"], \
        thropt_results["threshold"]
      
   #============================================================================
   # KSM and LSG-train
   #============================================================================
      
   else:
      info_msg("Running Kernel Space Mining")

      dictionary, opt_times["KSM"] = mine(options.outdir, 
         tk_models, options.threshold, options.min_frequency, 
         options.ncpu, get_etime = True)

      info_msg("Linearizing training data")
   
      lin_tr, lin_tr_mapping, opt_times["LSGtr"] = linearize_file(options.outdir, 
         dictionary, train, ncpu = options.ncpu, get_etime = True)
   
   #============================================================================
   # LSG-test
   #============================================================================

   info_msg("Linearizing test data")
   
   lin_te, lin_te_mapping, opt_times["LSGte"] = linearize_file(options.outdir, 
      dictionary, test, ncpu = options.ncpu, get_etime = True)
      
   #============================================================================
   # Linear space optimization
   #============================================================================
   
   if options.cost_factor == 0:

      info_msg("Optimizing cost factor ({0}-fold)".format(options.num_folds_cfopt))
   
      lslopt_out = optimize_cost_factor(options.outdir, 
         lin_tr, "linear", num_folds = options.num_folds_cfopt,
         ncpu = options.ncpu)
   
      options.cost_factor, opt_times["LSLopt"] = \
         lslopt_out["point"], lslopt_out["elapsed_time"]
   
   #============================================================================
   # Linear space learning
   #============================================================================

   info_msg("Learning model in the linear space")
      
   lin_model, opt_times["LSL"] = learn(options.outdir, lin_tr, 
      kernel_type = "linear", 
      cost_factor = options.cost_factor, 
      get_etime = True)
   
   #============================================================================
   # Classification
   #============================================================================

   info_msg("Classifying in the linear space")
   
   predictions, opt_times["LSC"] = classify(lin_te, lin_model, 
      get_etime = True)
   
   #============================================================================
   # OPT Results
   #============================================================================

   info_msg("Wrapping up")
   
   sort_features(dictionary, lin_model, lin_tr_mapping)
   
   eval = predictions + ".eval"
   opt_results = evaluate(predictions, lin_te, store = eval)
   
   result_msg("OPT precision: {0}".format(opt_results["precision"]))
   result_msg("OPT recall: {0}".format(opt_results["recall"]))
   result_msg("OPT F1 measure: {0}".format(opt_results["f1"]))
   
   for title, value in sorted(opt_times.items()):
      result_msg("OPT {0} time: {1:.2f}".format(title, value))
   result_msg("OPT total time: {0:.2f}".format(sum(opt_times.values())))
   
   num_frags = persistency.load(dictionary).num_leaves
   result_msg("OPT fragments: {0}".format(num_frags))
   
   result_msg("OPT KSM threshold: {0}".format(options.threshold))
   result_msg("OPT LSL cost factor: {0}".format(options.cost_factor))
   
   #============================================================================
   # TK and LIN Results
   #============================================================================
   
   if type(tk_models) != list:
      
      if options.tk_evaluate:

         info_msg("Evaluating TK model")
      
         tk_predictions, tk_class_time = classify(test, tk_models, 
            outdir = options.outdir, get_etime = True)
         tk_accuracy = evaluate(tk_predictions, test, store = predictions + ".eval")
         result_msg("TK precision: {0}".format(tk_accuracy["precision"]))
         result_msg("TK recall: {0}".format(tk_accuracy["recall"]))
         result_msg("TK F1 measure: {0}".format(tk_accuracy["f1"]))
         result_msg("TK class. time: {0:.2f}".format(tk_class_time))
         
         result_msg("TK gradient norm: {0}".format(
            get_tk_model_norm(tk_models)))
         
         tk_count_frags = count_frags_in_model(tk_models)["max"]
         result_msg("TK fragments: {0:.2g}".format(tk_count_frags))
         
      if options.lin_evaluate:

         info_msg("Evaluating LIN model")
      
         lin_tk_model = tk2lin(tk_models, dictionary,
            outdir = options.outdir, ncpu = options.ncpu)
         
         lin_tk_model_norm = calculate_linear_model_norm(lin_tk_model)
         
         lin_predictions, lin_class_time = classify(lin_te, lin_tk_model, 
            predictions = lin_te + ".pred_tk2lin", get_etime = True)
      
         eval = lin_predictions + ".eval"
         lin_results = evaluate(lin_predictions, lin_te, store = eval)
         
         result_msg("LIN precision: {0}".format(lin_results["precision"]))
         result_msg("LIN recall: {0}".format(lin_results["recall"]))
         result_msg("LIN F1 measure: {0}".format(lin_results["f1"]))
         result_msg("LIN class. time: {0:.2f}".format(lin_class_time))
         result_msg("LIN gradient norm: {0}".format(lin_tk_model_norm))

   return 0
   
if __name__ == "__main__":
   import sys
   main(sys.argv[1:])


#! /usr/bin/python
import sys, re, math, os

if __name__ == "__main__":
        co_occur = {}
        N = .0
        df = {} cf = {}
        score = {}
        inData = open(sys.argv[1], 'r')
        outData = open(sys.argv[2], 'w')
        p = re.compile('([01])\t(.*)')
        psp = re.compile(' ')
        for line in inData:
                m = p.match(line)
                if m:
                        N = N + 1
                        lab = m.group(1)
                        if lab not in cf:
                                cf[lab] = 1.0
                        else:
                                cf[lab] = cf[lab] + 1.0
                        cont = m.group(2)
                        tklst = psp.split(cont)
                        for ent in tklst:
                                if ent == '':
                                        continue
                                entlst = ent.split(':')
                                tk = entlst[0]
                                if tk not in df:
                                        df[tk] = 1.0
                                else:
                                        df[tk] = df[tk] + 1.0
                                if (tk, lab) not in co_occur:
                                        co_occur[tk,lab] = 1.0
                                else:
                                        co_occur[tk,lab] = co_occur[tk,lab] + 1.0
        for tk in df:
                s = 0
                for lb in cf:
                        A = 0.0001
                        if (tk,lb) in co_occur:
                                A = co_occur[tk,lb]
                        B = df[tk] - A 
                        C = cf[lb] - A
                        D = N + A - df[tk] - cf[lb]
                        i = cf[lb]/N
                        #print 'A=' + str(A)+':B='+str(B)+':C='+str(C)+':D='+str(D)
                        tmps = (A*D-C*B)*(A*D-C*B)*N/((A+C)*(B+D)*(A+B)*(C+D))
                        s = s + i*tmps 
                score[tk] = s
        rklst = []
        for tk in df:
                if tk == '':
                        continue
                rklst.append((-score[tk],tk))
        rklst.sort()
        for pr in rklst:
                outData.write('<F>'+pr[1]+'</F><V>'+str(-pr[0])+'</V>'+'\n')
        inData.close()
        outData.close()

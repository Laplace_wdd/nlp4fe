import sys, re, string, pickle, math
from itertools import *

def main():
	dataInVecF = open(sys.argv[1], 'r')
	dataOutVecF = open(sys.argv[2], 'w')
	genNewFeaId(dataInVecF, dataOutVecF)
	dataInVecF.close()
	dataOutVecF.close()

def genNewFeaId(dataInVecF, dataOutVecF):
	attrp = re.compile(r' (\S+)\s+(\d+) .*')
	linp = re.compile(r'(1|-1) (.*)')
	dictp = re.compile('(\d+):(.*)');
	labLst, feaLst, idf = [], [], {}
	for l in dataInVecF:
		m = linp.match(l) 
		lab = m.group(1)
		labLst += [lab]
		elemLst = m.group(2).split()
		curLst = []
		for fp in elemLst:
			fid = int(dictp.match(fp).group(1))
			val = float(dictp.match(fp).group(2))
			curLst += [(fid, val)]
			if fid in idf:
				idf[fid] += 1
			else:
				idf[fid] = 1.0 
		feaLst += [curLst]
	docN = len(labLst)
	for i in range(0, docN):
		print >> dataOutVecF, '{0} {1}'.format(labLst[i], ' '.join(['{0}:{1}'.format(str(fid), str(val*math.pow(math.log(docN/idf[fid]), 0))) for fid, val in feaLst[i]]))
	
if __name__ == "__main__": main()

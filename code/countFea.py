import sys, re, string, pickle
from itertools import *

def main():
	attrRankLstF = open(sys.argv[1], 'r')
	genNewFeaId(attrRankLstF)
	attrRankLstF.close()

def genNewFeaId(attrRankLstF):
	fTp, ftn = re.compile(r'^FRAME_FEATURE\-[A-Z][a-z_]+\-[a-z ]+'), 0
	fEp, fen = re.compile(r'^FRAME_FEATURE\-[A-Z][a-z_]+\-[A-Z][a-z_]+'), 0
	fNp, fnn = re.compile(r'^FRAME_FEATURE\-[A-Z][a-z_]+$'), 0
	poDp, podn= re.compile(r'^PRIOR_POLARITY_FEATURE\-'), 0
	unip, unin= re.compile(r'^[a-z]\S+$'), 0
	bip, bin= re.compile(r'^[a-z]\S+ [a-z]\S+$'), 0
	trip, trin= re.compile(r'^[a-z]\S+ [a-z]\S+ [a-z]\S+$'), 0
	treep, treen= re.compile(r'^TREE_FEATURE\-'), 0
#	frameDJJp = re.compile(r'^PRIOR_POLARITY_FEATURE\-JJ')
#	frameDVBp = re.compile(r'^PRIOR_POLARITY_FEATURE\-VB')
#	frameDRBp = re.compile(r'^PRIOR_POLARITY_FEATURE\-RB')
#	frameDAllp = re.compile(r'^PRIOR_POLARITY_FEATURE\-All')

	topN = int(sys.argv[2])
	linp = re.compile(r' \S+\s+\d+ (.*)')
	cnt = 0
	for l in attrRankLstF:
		m = linp.match(l)
		if m:
			cnt += 1
			if cnt > topN:break
			fea = m.group(1)
			if fTp.match(fea): ftn += 1 
			elif fEp.match(fea): fen += 1 
			elif fNp.match(fea): fnn += 1 
			elif poDp.match(fea): podn += 1 
			elif unip.match(fea): unin += 1 
			elif bip.match(fea): bin += 1 
			elif trip.match(fea): trin += 1 
			elif treep.match(fea): treen += 1 
			else:
				print fea
	print '{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}'.format(str(unin),str(bin),str(trin),str(ftn),str(fen),str(fnn),str(podn),str(treen))

if __name__ == "__main__": main()

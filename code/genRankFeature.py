import sys, re, string, pickle, math
from itertools import *

def main():
	dataInVecF = open(sys.argv[1], 'r')
	dataOutVecF = open(sys.argv[2], 'w')
	genNewFeaId(dataInVecF, dataOutVecF)
	dataInVecF.close()
	dataOutVecF.close()

def genNewFeaId(dataInVecF, dataOutVecF):
	linp = re.compile(r'(1|-1) (.*)')
	caseLst = []
	for l in dataInVecF:
		m = linp.match(l) 
		lab = int(m.group(1))
		elemS = m.group(2)
		caseLst += [(lab, elemS)]
	if sys.argv[3] == '1':
		caseLst.sort(reverse=True)
	for lab, elemS in caseLst:
		print >> dataOutVecF, '{0} qid:1 {1}'.format(lab, elemS)
	
if __name__ == "__main__": main()

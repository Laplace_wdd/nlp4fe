"""
This script works for combining linearized tree feature with the bag of words/frame into a
unified vector space model. ALso, it will generate the feature id-feature pair stored in a
attribute file
"""
import sys, re, string, pickle
from itertools import *

def main():
	idxF = open(sys.argv[1], 'r')
	invIdxF = open(sys.argv[2], 'w')
	genNewFeaId(idxF, invIdxF)
	idxF.close()
	invIdxF.close()

def genNewFeaId(idxF, invIdxF):
	docId = 0;
	invMap = {};
	linp = re.compile(r'(1|-1) (.*)')
	pap = re.compile(r'(.*):.*')
	for l in idxF:
		docId += 1
		m = linp.match(l)
		feaLst = m.group(2).split()
		for pa in feaLst:
			fea = pap.match(pa).group(1)
			if fea in invMap:
				invMap[fea] += [str(docId)] 
			else:
				invMap[fea] = [str(docId)]
	pickle.dump(invMap, open('invMap', 'w'))
	idLst = sorted([(len(invMap[fea]), fea) for fea in invMap], reverse = True)
	invIdxF.write('\n'.join(['{0}:{1}'.format(fea, ' '.join(invMap[fea])) for lenFea, fea in idLst]))

if __name__ == "__main__": main()

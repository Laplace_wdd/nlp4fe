"""
This script works for combining linearized tree feature with the bag of words/frame into a
unified vector space model. ALso, it will generate the feature id-feature pair stored in a
attribute file
"""
import sys, re, string
from itertools import *

idMap = {}

def main():
	attrInF = open(sys.argv[1], 'r')
	dictInF = open(sys.argv[2], 'r')
	attrOutF = open(sys.argv[3], 'w')
	dictOutF = open(sys.argv[4], 'w')
	genNewFeaId(attrInF, dictInF, attrOutF, dictOutF)
	attrInF.close()
	dictInF.close()
	attrOutF.close()
	dictOutF.close()

def genNewFeaId(attrInF, dictInF, attrOutF, dictOutF):
	id = 0;
	dictp = re.compile('(\d+):(.*)');
	for line in attrInF:
		id += 1;
		attrOutF.write(line.replace('\'','\\\''))
	idMap['1'] = id
	id += 1
	attrOutF.write('TREE_FEATURE-(TARGET_OBJECT)\n')
	feaLst = [(dictp.match(line).group(2).replace('\'','\\\''), dictp.match(line).group(1)) for line in dictInF]
	feaLst.sort()
	for fea, oid in feaLst:
		idMap[oid] = id
		id += 1
		attrOutF.write('TREE_FEATURE-{0}\n'.format(fea))
	dictOutF.write('\n'.join(['{0}:{1}'.format(str(id), str(idMap[id])) for id in idMap]));

if __name__ == "__main__": main()

import sys, re, math
from itertools import *
name = sys.argv[3]
def pAtN(lst, keySet, N):
	corr = 0.0
	for score, docId in lst[0:N]:
		if  docId in keySet:
			corr += 1
	return corr/N
	
def main():
	predF = open(sys.argv[1], 'r')
	keyF = open(sys.argv[2], 'r')
	linp = re.compile(r'(\S+) (.*)')
	rkLst = []
	posSet = set()
	negSet = set()
	docId = 0
	format = sys.argv[4]
	if format == 'svmlight':
		for l1, l2 in izip(predF, keyF):
			docId += 1
			rkLst += [(float(l1.strip()), docId)]
			lab = int(linp.match(l2).group(1))
			if lab == 1:
				posSet.add(docId)
			else:
				negSet.add(docId)
	if format == 'liblinear':
		keyp = re.compile(r'(1|-1) (\S+) (\S+)')
		for l1 in predF:
			m = keyp.match(l1)
			if m: 
				docId += 1
				l2 = keyF.readline() 
				lab = int(linp.match(l2).group(1))
				rkLst += [(float(m.group(2)), docId)]
				if lab == 1:
					posSet.add(docId)
				else:
					negSet.add(docId)
	rkLst.sort()
	print '{0},-1,{1}'.format(name, ','.join([str(pAtN(rkLst, negSet, k)) for k in [1, 2, 5, 10, 20, 50, 100, 200, 500, 1000]]))
	rkLst.reverse()
	print '{0},1,{1}'.format(name, ','.join([str(pAtN(rkLst, posSet, k)) for k in [1, 2, 5, 10, 20, 50, 100, 200, 500, 1000]]))
	predF.close()
	keyF.close()

if __name__ == "__main__": main()

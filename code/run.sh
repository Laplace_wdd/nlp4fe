rm ../model/*
python3 -m flink.linearize.ksl -o ../model -k STK ../data/train.data > log.txt
python3 -m flink.linearize.binary -t 1000000 -f 0 -k ../model -o ../model ../data/train.data ../data/$1.data > log.txt
cp ../model/dictionary.dict.txt ../data/dictionary.dict
cp ../model/$1.data.lin ../model/$1.data.lin ../data/
python mergeAttr.py ../data/train.attr ../data/dictionary.dict ../data/$1.all.attr ../data/idMap.txt 
python genVecWeka.py ../data/train.data ../data/train.data.lin ../data/train.all.attr ../data/idMap.txt ../data/train.all.arff 
python genVecLib.py ../data/$1.data ../data/$1.data.lin ../data/idMap.txt ../data/$1.all.vec 
python merge.py ../data/train.data ../data/train.data.lin ../data/train.attr ../data/dictionary.dict ../data/attr.txt ../data/train.all.data
python merge.py ../data/test.data ../data/test.data.lin ../data/train.attr ../data/dictionary.dict ../data/attr.txt ../data/test.all.data
python genWeka.py ../data/train.data ../data/train.data.lin ../data/train.attr ../data/dictionary.dict ../data/train.all.arff
mv ../model/*.pred ../model/*.pred.eval ../output/
#../../svm-light-TK-1_2_1/svm_learn -t 5 -c 1 -C + ../data/train.data ../model/model > ../output/log_tr.txt
#../../svm-light-TK-1_2_1/svm_classify ../data/test.data ../model/model ../output/test_ori.pred > ../output/log_tst.txt
#python genInvIdx.py ../data/train.all.vec ../data/invIdx.txt
#python genDocLst.py ../stat/infoGain.txt ../data/original/reuters_2008_gics50_p1y_stockprice_lag0_duration2_bigchange2pct_tof_bofte_tf_pp_train.doc ../stat/infoGainDocLst.txt 100
#python genDocLst.py ../stat/oneRAttr.txt ../data/original/reuters_2008_gics50_p1y_stockprice_lag0_duration2_bigchange2pct_tof_bofte_tf_pp_train.doc ../stat/oneRAttrDocLst.txt 100

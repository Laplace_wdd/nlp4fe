import sys, re, math
from itertools import *
name = sys.argv[3]
class ClassLabel:
	def __init__(self, labName):
		self.name = labName
		self.tp, self.tn, self.fp, self.fn = 0.0,0.0,0.0,0.0
	
	def update(self, pred, lab):
		if pred == self.name and lab == self.name:
			self.tp += 1
		elif pred == self.name and lab !=self.name:
			self.fp += 1
		elif pred != self.name and lab == self.name:
			self.fn += 1
		elif pred != self.name and lab != self.name:
			self.tn += 1
	
	def display(self):
		precision = self.tp/(self.tp+self.fp)
		recall = self.tp/(self.tp+self.fn)
		fscore = 2*precision*recall/(precision+recall)
		total_pred = self.tp + self.fp
		total_lab = self.tp + self.fn
		mcc = (self.tp*self.tn - self.fp*self.fn)/math.sqrt((self.tp+self.fp)*(self.tp+self.fn)*(self.tn+self.fp)*(self.tn+self.fn))
		acc = (self.tp+self.tn)/(self.tp+self.tn+self.fp+self.fn)
		print '{0},{1},{2},{3},{4},{5},{6},{7},{8}'.format(str(name), str(self.name), str(total_pred), str(total_lab), str(precision), str(recall), str(fscore), str(mcc), str(acc))

def main():
	predF = open(sys.argv[1], 'r')
	keyF = open(sys.argv[2], 'r')
	linp = re.compile(r'(\S+) (.*)')
	labLst = [ClassLabel(1),ClassLabel(-1)]
	format = sys.argv[4]
	if format == 'svmlight':
		for l1, l2 in izip(predF, keyF):
			pred = int(math.copysign(1,float(l1.strip())))
			lab = int(linp.match(l2).group(1))
			for labCase in labLst:
				labCase.update(pred, lab)
	if format == 'liblinear':
		keyp = re.compile(r'(1|-1) (\S+) (\S+)')
		for l1 in predF:
			m = keyp.match(l1)
			if m: 
				l2 = keyF.readline() 
				pred = int(m.group(1))
				lab = int(linp.match(l2).group(1))
				for labCase in labLst:
					labCase.update(pred, lab)
	predF.close()
	keyF.close()
	for labCase in labLst:
		labCase.display()

if __name__ == "__main__": main()

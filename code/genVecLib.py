"""
This script works for combining linearized tree feature with the bag of words/frame into a
unified vector space model. ALso, it will generate the feature id-feature pair stored in a
attribute file
"""
import sys, re, string
from itertools import *

idMap = {}

def main():
	combf = open(sys.argv[1], 'r')
	linf = open(sys.argv[2], 'r')
	dictf = open(sys.argv[3], 'r')
	libf = open(sys.argv[4], 'w')
	genNewFeaId(dictf, libf, linf, combf)
	dictf.close()
	combf.close()
	linf.close()
	libf.close()

def genNewFeaId(dictf, libf, linf, combf):
	id = 0;
	idMap = {};
	dictp = re.compile('(\d+):(.*)');
	for line in dictf:
		m = dictp.match(line);
		idMap[m.group(1)] = int(m.group(2));
	linp = re.compile(r'(1|-1) (.*)')
	cmbp = re.compile(r'.*\|BV\|(.*)\|EV\|')
	pap = re.compile(r'(.*):(.*)')
	for l1, l2 in izip(linf, combf):
		m1 = linp.match(l1)
		m2 = cmbp.match(l2)
		lab = m1.group(1)
		treeFea = m1.group(2).split()
		vecFea = m2.group(1).split()
		newFeaLst = [(int(pap.match(pa).group(1)), pap.match(pa).group(2)) for pa in vecFea];
		newFeaLst += [(idMap[pap.match(pa).group(1)]+1, pap.match(pa).group(2)) for pa in treeFea]
		newFeaLst.sort()
		libf.write('{1} {0}\n'.format(' '.join(['{0}:{1}'.format(fid, val) for (fid, val) in newFeaLst]), lab))

if __name__ == "__main__": main()

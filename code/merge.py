"""
This script works for combining linearized tree feature with the bag of words/frame into a
unified vector space model. ALso, it will generate the feature id-feature pair stored in a
attribute file
"""
import sys, re
from itertools import *

idMap = {}

def main():
	combf = open(sys.argv[1], 'r')
	linf = open(sys.argv[2], 'r')
	attrf = open(sys.argv[3], 'r')
	dictf = open(sys.argv[4], 'r')
	attr2f = open(sys.argv[5], 'w')
	lin2f = open(sys.argv[6], 'w')
	genNewFeaId(dictf, attrf, attr2f)
	dictf.close()
	attrf.close()
	attr2f.close()
	appendNewFeature(combf, linf, lin2f)
	combf.close()
	linf.close()
	lin2f.close()

def genNewFeaId(dictf, attrf, attr2f):
	id = 1;
	dictp = re.compile('(\d+):(.*)');
	for line in attrf:
		id += 1;
		attr2f.write(line);
	idMap['1'] = id
	id += 1
	attr2f.write('TREE_FEATURE-(TARGET_OBJECT)\n')
	for line in dictf:
		m = dictp.match(line)
		oid = m.group(1)
		idMap[oid] = id
		id += 1
		attr2f.write('TREE_FEATURE-{0}\n'.format(m.group(2)))

def appendNewFeature(combf, linf, lin2f):
	linp = re.compile(r'(1|-1) (.*)')
	cmbp = re.compile(r'.*\|BV\|(.*)\|EV\|')
	pap = re.compile(r'(.*):(.*)')
	for l1, l2 in izip(linf, combf):
		m1 = linp.match(l1)
		m2 = cmbp.match(l2)
		lab = m1.group(1)
		treeFea = m1.group(2).split()
		newFeaLst = [(idMap[pap.match(pa).group(1)], pap.match(pa).group(2)) for pa in treeFea]
		newFeaLst.sort()
		lin2f.write('{0}{1}{2}\n'.format(lab, m2.group(1), ' '.join(['{0}:{1}'.format(id, val) for (id, val) in newFeaLst])))

if __name__ == "__main__": main()

"""
This script works for combining linearized tree feature with the bag of words/frame into a
unified vector space model. ALso, it will generate the feature id-feature pair stored in a
attribute file
"""
import sys, re, string, pickle
from itertools import *

def main():
	attrRankLstF = open(sys.argv[1], 'r')
	docLstF = open(sys.argv[2], 'r')
	docF = open(sys.argv[3], 'w')
	genNewFeaId(docLstF, attrRankLstF, docF)
	docLstF.close()
	attrRankLstF.close()
	docF.close()

def genNewFeaId(docLstF, attrRankLstF, docF):
	topN = int(sys.argv[4])
	invMap = pickle.load(open("invMap.txt", "r"));
	linp = re.compile(r' (\S+)\s+(\d+) .*')
	docLst = [l.strip() for l in docLstF]
	cnt = 0
	for l in attrRankLstF:
		m = linp.match(l)
		if m:
			cnt += 1
			if cnt == topN:break
			score = float(m.group(1))
			id = m.group(2)
			docF.write(l+'{\n')
			docF.write('\n'.join(docLst[int(i)-1] for i in invMap[id]))
			docF.write('\n}\n')
if __name__ == "__main__": main()
